package Ariel;
import org.junit.Assert;
import org.junit.Test;

public class PersonaTest {
	@Test
    public void crearPersona() {
		long dni = 35730982;
		//long otrodni= 40230230;
		Persona personaUno = new Persona(dni,"Juan","Av Corrientes 123");
		//String st = "hola";
		//Persona personaDos = new Persona(otrodni,"Pepe","Av Libertador 123");
        
		Assert.assertEquals(dni, personaUno.getDni());

        Assert.assertEquals("Juan", personaUno.getNombre());
        
        personaUno.setNombre("Carlos");
        Assert.assertEquals("Carlos", personaUno.getNombre());

        personaUno.setNombre("Pepe");
        Assert.assertEquals("Pepe", personaUno.getNombre());
        
        Assert.assertEquals("", personaUno.getNombre());
        
        
        Assert.assertEquals("Av Corrientes 123", personaUno.getDomicilio());
        
        personaUno.setDomicilio("Av Libertador 123");
        Assert.assertEquals("Av Libertador 123", personaUno.getDomicilio());
    
        personaUno.setDomicilio("Callao 123");
        Assert.assertEquals("Callao 123", personaUno.getDomicilio());
        
        
    }
}
