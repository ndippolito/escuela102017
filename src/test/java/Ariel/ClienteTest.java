package Ariel;


import org.junit.Test;

import dipi.MontoNegativoCuentaException;

import static org.junit.Assert.*;

public class ClienteTest {
	

	@Test(expected =  NombreNoValidoException.class )
	public void testGetNombre() throws NombreNoValidoException {
		assertEquals(null,new Cliente("NombreCliente11111").getNombre());
		assertEquals(null,new Cliente("NombreCliente@@@ł€@ł€@łđðßßð·@~·@").getNombre());
		assertEquals(null,new Cliente("Andrés").getNombre());	
		assertEquals("Andres",new Cliente("Andres").getNombre());
		assertEquals(null,new Cliente("NombreCliente3").getNombre());	
		assertEquals("Carlos",new Cliente("Carlos").getNombre());
		
	}
	@Test
	public void testAgregarCuenta() throws NombreNoValidoException, MontoNegativoCuentaException { //Dudas Consultar
		Cliente cli1 = new Cliente("Carlos");
		Cliente cli2 = new Cliente("Carlos");
		Cuenta cuenta1= new CuentaCorriente(1,20000,cli1);

		Cuenta cuenta2= new CuentaCorriente(1,20000,cli1);
		cli1.agregarCuenta(cuenta1);
		cli2.agregarCuenta(cuenta1);
		assertEquals(cli1.getCuentas(),cli2.getCuentas());
		
		cli2.agregarCuenta(cuenta2);
		cli1.agregarCuenta(cuenta2);
		assertEquals(cli1.getCuentas(),cli2.getCuentas());
		

		Cuenta cuenta10= new CajaDeAhorro(1,20000,cli1);

		Cuenta cuenta20= new CajaDeAhorro(1,20000,cli1);
		cli1.agregarCuenta(cuenta10);
		cli2.agregarCuenta(cuenta10);
		assertEquals(cli1.getCuentas(),cli2.getCuentas());
		
		cli2.agregarCuenta(cuenta20);
		cli1.agregarCuenta(cuenta20);
		assertEquals(cli1.getCuentas(),cli2.getCuentas());
		
	}
	
	@Test
	public void testEquals() throws Exception {
		Object cli1 = new Cliente("Carlos");
		Object cli2 = new Cliente("Carlos");
		
		assertEquals(cli1, cli2);
		assertNotEquals(cli1, null);
		assertNotEquals(cli1, new Object());

	}

	
	
	
	
	
	
}
