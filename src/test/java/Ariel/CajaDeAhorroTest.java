package Ariel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import dipi.MontoNegativoCuentaException;


public class CajaDeAhorroTest {

	@Test(expected = MontoNegativoCuentaException.class)
	public void testGetId() throws NombreNoValidoException, MontoNegativoCuentaException {
		Cliente cli1 = new Cliente("Jorge");
		
		assertEquals(new CajaDeAhorro(1,25000,cli1).getId(),1);	
		assertEquals(new CajaDeAhorro(2,34000,cli1).getId(),2);
		assertEquals(new CajaDeAhorro(-1,34000,cli1).getId(),-1);

	}
	@Test
	public void testGetCliente() throws NombreNoValidoException, MontoNegativoCuentaException {
		Cliente cli1 = new Cliente("Jorge");
		Cliente cli2 = new Cliente("Pepe");
		assertEquals(new CajaDeAhorro(1,25000,cli1).getCliente(),cli1);			
		assertEquals(new CajaDeAhorro(1,45000,cli2).getCliente() , cli2);	

	}
	@Test
	public void testGetSaldo() throws NombreNoValidoException, MontoNegativoCuentaException {
		Cliente cli1 = new Cliente("Jorge");
		assertEquals(new CajaDeAhorro(1,25000,cli1).getSaldo() , 25000);			
		assertEquals(new CajaDeAhorro(1,45000,cli1).getSaldo() , 45000);	
		assertEquals(new CajaDeAhorro(1,-45000,cli1).getSaldo() , 0);	

	}
	@Test(expected = MontoNegativoCuentaException.class)
	public void testDepositar() throws MontoNegativoCuentaException, NombreNoValidoException {
		Cliente cli1 = new Cliente("Jorge");
		assertEquals(new CajaDeAhorro(1,25000,cli1).depositar(20000), true);			
		assertEquals(new CajaDeAhorro(1,45000,cli1).depositar(10000) , true);	
		assertEquals(new CajaDeAhorro(1,-45000,cli1).depositar(-98000) , false);
		assertEquals(new CajaDeAhorro(1,-45000,cli1).depositar(-18000) , false);	

	}
	@Test
	public void testExtraer() throws MontoNegativoCuentaException, NombreNoValidoException {
		Cliente cli1 = new Cliente("Jorge");
		assertEquals(new CajaDeAhorro(1,25000,cli1).extraer(20000), true);			
		assertEquals(new CajaDeAhorro(1,45000,cli1).extraer(10000) , true);	
		assertEquals(new CajaDeAhorro(1,-45000,cli1).extraer(-98000) , false);
		assertEquals(new CajaDeAhorro(1,-45000,cli1).extraer(-18000) , false);	
		assertEquals(new CajaDeAhorro(1,-45000,cli1).extraer(0) , true);	
		assertEquals(new CajaDeAhorro(1,45000,cli1).extraer(45000) , true);	

	}
	@Test
	public void testEquals() throws MontoNegativoCuentaException, NombreNoValidoException {
		Cliente cli1 = new Cliente("Jorge");
		CajaDeAhorro caja1 = new CajaDeAhorro(1,5000,cli1);
		CajaDeAhorro caja2 = new CajaDeAhorro(1,25000,cli1);
		
		assertNotEquals(caja1,caja2);
		assertEquals(caja1,caja1);
		assertEquals(caja2,caja2);
	}
	
}
