package AjedrezAriel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class TableroTest {
	@Test
	public void testJugador()  {
		
		Jugador jugador1 = new Jugador("Carlos");
		Jugador jugador2 = new Jugador("Pepe");
		Ajedrez tablero = new Ajedrez();
		tablero.agregarJugador1(jugador1);
		assertEquals(jugador1,tablero.getJugador1());
		tablero.agregarJugador2(jugador2);
		assertEquals(jugador2,tablero.getJugador2());
		assertNotEquals(jugador2,tablero.getJugador1());
		assertNotEquals(jugador1,tablero.getJugador2());
		
	}
	
}
