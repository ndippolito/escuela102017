package Ariel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class BancoTest {
	@Test(expected =  NombreNoValidoException.class )
	public void testGetNombre() throws NombreNoValidoException {
		assertEquals(null,new Banco("NombreCliente11111").getNombre());
		assertEquals(null,new Banco("NombreCliente@@@ł€@ł€@łđðßßð·@~·@").getNombre());
		assertEquals(null,new Banco("Andrés").getNombre());	
		assertEquals("Andres",new Banco("Andres").getNombre());
		assertEquals(null,new Banco("NombreCliente3").getNombre());	
		assertEquals("Carlos",new Banco("Carlos").getNombre());
		
	}
	@Test(expected =  NombreNoValidoException.class )
	public void testAgregarCliente() throws NombreNoValidoException {
		Cliente cli1= new Cliente("NombreCliente11111");
		Cliente cli2= new Cliente("NombreCliente11112");
		Cliente cli3= new Cliente("NombreCliente11113");
		Banco banco1 = new Banco("Ariel");

		Set<ICliente>clientes = new HashSet<ICliente>();

		assertEquals( clientes , banco1.getClientes());
		
		clientes.add(cli3);
		
		banco1.agregarCliente(cli3);
		assertEquals( clientes , banco1.getClientes());
		clientes.add(cli2);
		
		banco1.agregarCliente(cli1);
		assertNotEquals( clientes , banco1.getClientes());
		
	}
	@Test(expected =  NombreNoValidoException.class )
	public void testAgregarCuenta() throws NombreNoValidoException, MontoNegativoCuentaException {
		Cliente cli1 = new Cliente("Jorge");
		
		CuentaCorriente caja1 = new CuentaCorriente(1,5000,cli1);
		CuentaCorriente caja2 = new CuentaCorriente(1,25000,cli1);
		CajaDeAhorro caja3 = new CajaDeAhorro(1,225000,cli1);
		
		Banco banco1 = new Banco("andres1");

		Set<ICuenta> cuentas= new HashSet<ICuenta>();

		assertEquals( cuentas , banco1.getCuentas());
		
		cli1.agregarCuenta(caja3);
		cuentas.add(caja3);
		
		banco1.agregarCliente(cli1);
		assertEquals( cuentas , banco1.getCuentas());
		
		cli1.agregarCuenta(caja1);
		cuentas.add(caja2);
		
		banco1.agregarCliente(cli1);
		assertEquals( cuentas , banco1.getCuentas());
	}
}
