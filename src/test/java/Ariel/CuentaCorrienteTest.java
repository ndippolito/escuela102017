package Ariel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import dipi.MontoNegativoCuentaException;

public class CuentaCorrienteTest {
	@Test(expected =  MontoNegativoCuentaException.class )
	public void testGetId() throws NombreNoValidoException, MontoNegativoCuentaException {
		Cliente cli1 = new Cliente("Jorge");
		assertEquals(1,new CuentaCorriente(1,25000,cli1).getId());	
		assertEquals(2,new CuentaCorriente(2,34000,cli1).getId());
		assertEquals(0,new CuentaCorriente(-1,34000,cli1).getId());
	}
	@Test(expected =  MontoNegativoCuentaException.class )
	public void testGetGiroEnDescubierto() throws NombreNoValidoException, MontoNegativoCuentaException {
		Cliente cli1 = new Cliente("Jorge");
		assertEquals(1000,new CuentaCorriente(1,2000,cli1).giroEnDescubiertoHabilidado());	
		assertEquals(2000,new CuentaCorriente(2,4000,cli1).giroEnDescubiertoHabilidado());
		assertEquals(17000,new CuentaCorriente(-1,34000,cli1).giroEnDescubiertoHabilidado());
	}
	@Test
	public void testGetCliente() throws NombreNoValidoException, MontoNegativoCuentaException {
		Cliente cli1 = new Cliente("Jorge");
		Cliente cli2 = new Cliente("Pepe");
		assertEquals(cli1,new CuentaCorriente(1,25000,cli1).getCliente() );			
		assertEquals(cli2,new CuentaCorriente(1,45000,cli2).getCliente() );	

	}

	@Test(expected =  MontoNegativoCuentaException.class )
	public void testGetSaldo() throws MontoNegativoCuentaException, NombreNoValidoException {
		Cliente cli1 = new Cliente("Jorge");
		assertEquals(25000,new CuentaCorriente(1,25000,cli1).getSaldo());			
		assertEquals(45000, new CuentaCorriente(1,45000,cli1).getSaldo());	
		assertEquals(00, new CuentaCorriente(1,-45000,cli1).getSaldo());
		
		CuentaCorriente cuenta1 = new CuentaCorriente(1,-45000,cli1);
		cuenta1.extraer(1);
		assertEquals( 0, cuenta1.getSaldo());
		
		CuentaCorriente cuenta2 = new CuentaCorriente(1,40000,cli1);
		cuenta2.extraer(60000);
		assertEquals( -20000 , cuenta2.getSaldo());
		
		CuentaCorriente cuenta3 = new CuentaCorriente(1,40000,cli1);
		cuenta3.extraer(60001);
		assertEquals( 40000 ,cuenta3.getSaldo() );
		
		CuentaCorriente cuenta4 = new CuentaCorriente(1,40000,cli1);
		cuenta4.extraer(59999);
		assertEquals( -19999 , cuenta4.getSaldo());
		
	}
	
	@Test(expected =  MontoNegativoCuentaException.class )
	public void testDepositar() throws MontoNegativoCuentaException, NombreNoValidoException {
		Cliente cli1 = new Cliente("Jorge");
		assertEquals(new CuentaCorriente(1,25000,cli1).depositar(20000), true);			
		assertEquals(new CuentaCorriente(1,45000,cli1).depositar(10000) , true);	
		assertEquals(new CuentaCorriente(1,-45000,cli1).depositar(-98000) , false);
		assertEquals(new CuentaCorriente(1,-45000,cli1).depositar(-18000) , false);	

	}
	@Test(expected =  MontoNegativoCuentaException.class )
	public void testExtraer() throws MontoNegativoCuentaException, NombreNoValidoException {
		Cliente cli1 = new Cliente("Jorge");
		assertEquals(new CuentaCorriente(1,25000,cli1).extraer(20000), true);			
		assertEquals(new CuentaCorriente(1,45000,cli1).extraer(10000) , true);	
		assertEquals(new CuentaCorriente(1,-45000,cli1).extraer(-98000) , false);
		assertEquals(new CuentaCorriente(1,-45000,cli1).extraer(-18000) , false);	
		assertEquals(new CuentaCorriente(1, 10000 ,cli1).extraer(15000) , true);	
		assertEquals(new CuentaCorriente(1,45000,cli1).extraer(55000) , true);	

	}
	@Test
	public void testEquals() throws MontoNegativoCuentaException, NombreNoValidoException {
		Cliente cli1 = new Cliente("Jorge");
		CuentaCorriente caja1 = new CuentaCorriente(1,5000,cli1);
		CuentaCorriente caja2 = new CuentaCorriente(1,25000,cli1);
		CajaDeAhorro caja3 = new CajaDeAhorro(1,225000,cli1);
		
		assertNotEquals(caja3,caja2);
		assertNotEquals(caja1,caja2);
		assertEquals(caja1,caja1);
		assertEquals(caja2,caja2);
	}
}
