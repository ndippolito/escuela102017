package ajedrezPamela;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestFigura {

	@Test
	public void testVerificarLimiteMovimiento() {
		
		Figura figura = new Figura("Blanco");
		
		assertFalse(figura.VerificarLimiteMovimiento(0,8));
		assertFalse(figura.VerificarLimiteMovimiento(8,2));
		
		assertTrue(figura.VerificarLimiteMovimiento(7,5));
	}
	
	@Test
	public void testverificarDisponibilidadCasilla() {
		
		Tablero tablero = new Tablero();
		Jugador jugadorUno = new Jugador("Blanco");
		
		assertEquals(null,tablero.getTablero()[2][5]);
		assertTrue(tablero.getTablero()[1][0].verificarDisponibilidadCasilla(tablero, 2, 5));
		
		
		assertNotEquals(tablero.getTablero()[1][0].getColor(), tablero.getTablero()[6][7].getColor());
		assertTrue(tablero.getTablero()[1][0].verificarDisponibilidadCasilla(tablero, 6, 7));
	
		assertEquals(tablero.getTablero()[6][0].getColor(), tablero.getTablero()[6][1].getColor());
		assertFalse(tablero.getTablero()[6][0].verificarDisponibilidadCasilla(tablero, 6, 1));
		
	}

}
