package ajedrezPamela;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestJugador {

	@Test
	public void testMoverFigura() {
		

		Tablero tablero = new Tablero();
		Jugador jugadorUno = new Jugador("Blanco");
		
		
		assertTrue(jugadorUno.MoverFigura(tablero,1,0,6, 0));
		
		//jugadorUno intenta mover una figura que ya no esta en ese lugar
		assertFalse(jugadorUno.MoverFigura(tablero,1,0 , 1, 1));
		assertEquals(null, tablero.getTablero()[1][0]);
		assertEquals("Blanco", tablero.getTablero()[6][0].getColor());
		
		//jugadorUno intenta mover figura negra
		assertEquals("Negro", tablero.getTablero()[6][5].getColor());
		assertFalse(jugadorUno.MoverFigura(tablero, 6,5, 1, 1));	
		
	}

}
