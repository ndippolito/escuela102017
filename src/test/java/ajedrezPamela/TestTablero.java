package ajedrezPamela;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestTablero {

	@Test
	public void test() {
		Tablero tablero = new Tablero();
		Figura figuras[][] = tablero.getTablero();

		assertEquals("Blanco", figuras[1][0].getColor());
		assertEquals("Negro", figuras[6][0].getColor());
	
	}

}
