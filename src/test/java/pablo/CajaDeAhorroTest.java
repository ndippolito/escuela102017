package pablo;

import static org.junit.Assert.*;

import org.junit.Test;

import dylan.Banco;

public class CajaDeAhorroTest {

	@Test
	public void CuentaCorrienteTestCrearCuenta() {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		CajaDeAhorro unaCuenta = new CajaDeAhorro(001, unCliente);
		assertEquals(unaCuenta.getCliente(), unCliente);
		assertEquals(unaCuenta.getSaldo(), 0);
	}
	
	@Test
	public void CajaDeAhorroTest_depositarYExtraer() throws dipi.MontoNegativoCuentaException {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		CajaDeAhorro unaCuenta = new CajaDeAhorro(001, unCliente);
		unaCuenta.depositar(200);
		assertEquals(unaCuenta.getSaldo(), 200);
		unaCuenta.extraer(400);
		assertEquals(unaCuenta.getSaldo(), 200);
		unaCuenta.extraer(0);
		assertEquals(unaCuenta.getSaldo(), 200);
	}
	
	@Test
	public void cajaDeAhorroTestEquals() throws dipi.MontoNegativoCuentaException {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		Cliente otroCliente = new Cliente(12345, "Mr Brown", "Main St");
		Object unaCuenta = new CajaDeAhorro(001, unCliente);
		CajaDeAhorro otraCuenta = new CajaDeAhorro(001, unCliente);
		CajaDeAhorro thirdCuenta = new CajaDeAhorro(001, otroCliente);
		CajaDeAhorro fourthCuenta = new CajaDeAhorro(002, unCliente);
		assertTrue(unaCuenta.equals(otraCuenta));
		assertTrue(unaCuenta.equals(thirdCuenta));
		assertFalse(unaCuenta.equals(fourthCuenta));
		Banco bancoDylan = new Banco("Dylan Bank");
		assertNotEquals(bancoDylan, unaCuenta);
	}
	
	@Test
	public void testEqualsEnClase() throws Exception {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		CajaDeAhorro unaCuenta = new CajaDeAhorro(001, unCliente);
		assertNotEquals(unaCuenta, null);
		
		assertNotEquals(unaCuenta, unCliente);
		
	}

	
	
	
	
	
	
	
	
}
