package pablo;

import static org.junit.Assert.*;

import org.junit.Test;

public class ClienteTest {

	@Test
	public void clienteTest_crearClientes() {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		assertEquals(unCliente.getDNI(), 12345);
		assertEquals(unCliente.getNombre(), "Mr Brown");
		assertEquals(unCliente.getDomicilio(), "Main St");
		
		Cliente otroCliente = new Cliente(1111, "Mr! White", "First St");
		assertEquals(otroCliente.getDNI(), 1111);
		assertEquals(otroCliente.getNombre(), "");
		assertEquals(otroCliente.getDomicilio(), "First St");
	}
	
	@Test
	public void clienteTest_agregarCuentasValidas() {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		Cliente otroCliente = new Cliente(1111, "Mr White", "First St");
		Cuenta unaCuenta = new CajaDeAhorro(0001, unCliente);
		Cuenta otraCuenta = new CuentaCorriente(0002, otroCliente, 300);
		assertEquals(unaCuenta.getCliente(), unCliente);
		unCliente.agregarCuenta(unaCuenta);
		assertFalse(unCliente.getCuentas().isEmpty()); 
		otroCliente.agregarCuenta(otraCuenta);
		assertFalse(otroCliente.getCuentas().isEmpty());
	}
	
	@Test
	public void clienteTest_agregarCuentasInvalidas() {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		Cliente otroCliente = new Cliente(1111, "Mr White", "First St");
		Cuenta unaCuenta = new CajaDeAhorro(0001, otroCliente);
		Cuenta otraCuenta = new CuentaCorriente(0002, unCliente, 300);
		unCliente.agregarCuenta(unaCuenta);
		assertTrue(unCliente.getCuentas().isEmpty()); 
		otroCliente.agregarCuenta(otraCuenta);
		assertTrue(otroCliente.getCuentas().isEmpty());
	}
	
	@Test
	public void clienteTest_equals() {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		Cliente otroCliente = new Cliente(12345, "Mr Brown", "Main St");
		assertTrue(unCliente.equals(otroCliente));
		Cuenta unaCuenta = new CajaDeAhorro(0001, new Cliente(12345, "Mr Brown", "Main St"));
		unCliente.agregarCuenta(unaCuenta);
		otroCliente.agregarCuenta(unaCuenta);
		assertEquals(unCliente.getCuentas().size(), 1);
		assertEquals(otroCliente.getCuentas().size(), 1);
		assertTrue(unCliente.equals(otroCliente));
		
	}

}