package pablo;

import static org.junit.Assert.*;

import org.junit.Test;

public class BancoTest {

	@Test
	public void BancoTest_crearBanco() {
		Banco unBanco = new Banco("JP Morgan");
		assertEquals(unBanco.getNombre(), "JP Morgan");
	}
	
	@Test
	public void BancoTest_agregarClientes() {
		Banco unBanco = new Banco("JP Morgan");
		
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		Cliente otroCliente = new Cliente(1111, "Mr White", "First St");
		Cuenta unaCuenta = new CajaDeAhorro(0001, unCliente);
		Cuenta otraCuenta = new CuentaCorriente(0002, otroCliente, 300);
		Cuenta thirdCuenta = new CajaDeAhorro(0003, otroCliente);
		unCliente.agregarCuenta(unaCuenta);
		otroCliente.agregarCuenta(otraCuenta);
		otroCliente.agregarCuenta(thirdCuenta);
		assertTrue(unBanco.getClientes().isEmpty());
		assertTrue(unBanco.getCuentas().isEmpty());
		unBanco.agregarCliente(unCliente);
		unBanco.agregarCliente(otroCliente);
		assertEquals(unBanco.getClientes().size(), 2);
		assertEquals(unBanco.getCuentas().size(), 3);
	}
	
	@Test
	public void BancoTest_equals() {
		Banco unBanco = new Banco("JP Morgan");
		Banco otroBanco = new Banco("JP Morgan");
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		Cuenta unaCuenta = new CajaDeAhorro(0001, unCliente);
		unCliente.agregarCuenta(unaCuenta);

		assertTrue(unBanco.equals(otroBanco));
		unBanco.agregarCliente(unCliente);
		assertFalse(unBanco.equals(otroBanco));
		otroBanco.agregarCliente(unCliente);
		assertTrue(unBanco.equals(otroBanco));

	}
	

}
