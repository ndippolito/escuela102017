package pablo;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonaTest {

	@Test
	public void PersonaTest_crearPersonas() {
		Persona unaPers = new Persona(10101010);
		Persona otraPers = new Persona(20202020, "Mr Brown", "Unknown");
		
		assertEquals(unaPers.getDNI(), 10101010);
		assertEquals(unaPers.getNombre(), "");
		assertEquals(unaPers.getDomicilio(), "");
		assertEquals(otraPers.getDNI(), 20202020);
		assertEquals(otraPers.getNombre(), "Mr Brown");
		assertEquals(otraPers.getDomicilio(), "Unknown");
	}
	
	@Test
	public void PersonaTest_setters() {
		Persona unaPers = new Persona(10101010);
		Persona otraPers = new Persona(20202020, "Mr Brown", "Unknown");
		unaPers.setNombre("Mr Pink");
		unaPers.setDomicilio("Epidata");
		otraPers.setNombre("Mr White");
		otraPers.setDomicilio("Globant");
		
		assertEquals(unaPers.getDNI(), 10101010);
		assertEquals(unaPers.getNombre(), "Mr Pink");
		assertEquals(unaPers.getDomicilio(),"Epidata");
		assertEquals(otraPers.getDNI(), 20202020);
		assertEquals(otraPers.getNombre(), "Mr White");
		assertEquals(otraPers.getDomicilio(), "Globant");
	}
	
	@Test
	public void PersonaTest_equals() {
		Persona unaPers = new Persona(10101010);
		Persona otraPers = new Persona(20202020, "Mr Brown", "Unknown");
		boolean noSonIguales = unaPers.equals(otraPers);
		
		assertEquals(noSonIguales, false);
		Persona thirdPers = new Persona(10101010);
		boolean sonIguales = unaPers.equals(thirdPers);
		assertEquals(sonIguales, true);
		
		
		
		
	}


}
