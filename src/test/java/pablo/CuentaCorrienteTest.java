package pablo;

import static org.junit.Assert.*;

import org.junit.Test;

public class CuentaCorrienteTest {

	@Test
	public void CuentaCorrienteTest_crearCuenta() {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		CuentaCorriente unaCuenta = new CuentaCorriente(001, unCliente, 300);
		assertEquals(unaCuenta.getCliente(), unCliente);
		assertEquals(unaCuenta.getSaldo(), 0);
		assertEquals(unaCuenta.giroEnDescubiertoHabilidado(), 300);
	}
	
	@Test
	public void CuentaCorrienteTest_depositarYExtraer() throws dipi.MontoNegativoCuentaException {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		CuentaCorriente unaCuenta = new CuentaCorriente(001, unCliente, 300);
		unaCuenta.depositar(200);
		assertEquals(unaCuenta.getSaldo(), 200);
		unaCuenta.extraer(400);
		assertEquals(unaCuenta.getSaldo(), -200);
		unaCuenta.extraer(200);
		assertEquals(unaCuenta.getSaldo(), -200);
		
	}
	
	@Test
	public void CuentaCorrienteTest_equals() throws dipi.MontoNegativoCuentaException {
		Cliente unCliente = new Cliente(12345, "Mr Brown", "Main St");
		Cliente otroCliente = new Cliente(12345, "Mr Brown", "Main St");
		CuentaCorriente unaCuenta = new CuentaCorriente(001, unCliente, 200);
		CuentaCorriente otraCuenta = new CuentaCorriente(001, unCliente, 200);
		CuentaCorriente thirdCuenta = new CuentaCorriente(001, otroCliente, 200);
		CuentaCorriente fourthCuenta = new CuentaCorriente(002, unCliente, 200);
		assertTrue(unaCuenta.equals(otraCuenta));
		assertTrue(unaCuenta.equals(thirdCuenta));
		assertFalse(unaCuenta.equals(fourthCuenta));
	}
}
