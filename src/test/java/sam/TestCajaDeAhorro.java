package sam;

import static org.junit.Assert.*;

import org.junit.Test;

import dipi.ICliente;


public class TestCajaDeAhorro {
	@Test
	public void testCajaDeAhorro(){
		long id = 100;
		Cliente cliente = new Cliente();
		long saldo = 100;
		
		CajaDeAhorro cajaDeAhorro = new CajaDeAhorro(id, cliente, saldo);
		
		assertEquals(id, cajaDeAhorro.getId());
		assertEquals(cliente, cajaDeAhorro.getCliente());
		assertEquals(saldo, cajaDeAhorro.getSaldo());
		
		id = -100;
		// cliente = new Cliente();
		saldo = -100;
		
		cajaDeAhorro = new CajaDeAhorro(id, cliente, saldo);

		assertEquals(0, cajaDeAhorro.getId());
		// assertEquals(cliente, cajaDeAhorro.getCliente());
		assertEquals(0, cajaDeAhorro.getSaldo());
		
	}
	
}
