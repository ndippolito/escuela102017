package sam.ajedrez;

import org.junit.Test;

public class caballoTest {
	
	private caballo caballo;
	private boolean disponible;
	private int y;
	private int x;

	@Test
	public void testMovValido() throws Exception {
		caballo caballo = new caballo(false, 1, 2);
		(caballo).testMovValido(false, 3, 4);
	}
	
	@Test
	public void testPosicion(){
		setDisponible(true);
		setX(8);
		setY(6);
		
		setCaballo(new caballo(false, x, y));		
		assertEquals(false, 3,6);		
			
	}

	private void assertEquals(boolean b, int x, int y) {
		
	}

	public caballo getCaballo() {
		return caballo;
	}

	public void setCaballo(caballo caballo) {
		this.caballo = caballo;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

}