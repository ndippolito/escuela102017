package franco;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import dipi.IBanco;
import dipi.ICliente;
import dipi.ICuenta;

public class BancoTest {
	
	private IBanco banco;
	
	private String nombreEsperado;
	private Set<ICliente> clientesEsperados;
	private Set<ICuenta> cuentasEsperadas;
	
	private Cliente cliente1;
	private Cliente cliente2;
	
	@Before
	public void setup() throws Exception {
		nombreEsperado = "Nombre";
		
		cliente1 = new Cliente("Hernan", null);
		cliente2 = new Cliente("Lautaro", null);
		
		Cuenta cuenta1 = new CajaDeAhorro(1, cliente1, 10000);
		Cuenta cuenta2 = new CuentaCorriente(2, cliente2, 20000, 1000);		
		cuentasEsperadas = new HashSet<ICuenta>();
		cuentasEsperadas.add(cuenta1);
		cuentasEsperadas.add(cuenta2);		
		
		cliente1.agregarCuenta(cuenta1);
		cliente2.agregarCuenta(cuenta2);
		
		clientesEsperados = new HashSet<ICliente>();
		clientesEsperados.add(cliente1);
		clientesEsperados.add(cliente2);
	}
	
	@Test
	public void testCrearBanco() throws Exception {
		banco = new Banco(nombreEsperado, clientesEsperados);
		
		assertEquals(nombreEsperado, banco.getNombre());
		assertArrayEquals
			(clientesEsperados.toArray(), banco.getClientes().toArray());
		assertArrayEquals
			(cuentasEsperadas.toArray(), banco.getCuentas().toArray());
	}
	
	@Test(expected = NombreInvalidoException.class)
	public void testCrearBancoConNombreNulo() throws Exception {
		
		banco = new Banco(null, clientesEsperados);
	}
	
	@Test
	public void testCrearBancoConClientesNulos() throws Exception {
		int cantidadClientesEsperada = 0;		
		
		banco = new Banco(nombreEsperado, null);
		
		assertNotNull(banco.getClientes());
		assertEquals(cantidadClientesEsperada, banco.getClientes().size());
	}
	
	@Test
	public void testObtenerCuentasSinClientes() throws Exception {
		int cantidadCuentasEsperada = 0;
		
		banco = new Banco(nombreEsperado, null);
		
		assertNotNull(banco.getCuentas());
		assertEquals(cantidadCuentasEsperada, banco.getCuentas().size());
	}
	
	@Test
	public void testAgregarCliente() throws Exception {
		banco = new Banco(nombreEsperado, new HashSet<ICliente>());
		
		banco.agregarCliente(cliente1);
		banco.agregarCliente(cliente2);
		
		assertArrayEquals
			(clientesEsperados.toArray(), banco.getClientes().toArray());	
	}
	
	@Test
	public void testToString() throws Exception {
		banco = new Banco(nombreEsperado, null);
		
		assertEquals(nombreEsperado, banco.toString());
	}
	
	@Test
	public void testEquals() throws Exception {
		banco = new Banco(nombreEsperado, null);
		Banco otroBanco = new Banco(nombreEsperado, null);
		
		assertEquals(banco, otroBanco);
		
		String otroNombre = "Otro Nombre";
		otroBanco = new Banco(otroNombre, null);
		
		assertNotEquals(banco, otroBanco);
	}
}
