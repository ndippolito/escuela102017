package franco;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import dipi.ICliente;
import dipi.ICuenta;
import dipi.ICuentaCorriente;
import dipi.MontoNegativoCuentaException;

public class CuentaCorrienteTest {	
	
	private ICuentaCorriente cuenta;
	
	private long idEsperado;
	private ICliente clienteEsperado;
	private long saldoEsperado;
	private long giroEsperado;	
	
	@Before
	public void setup() throws Exception {
		cuenta = null;
		idEsperado = 123;
		clienteEsperado = new Cliente("Hernan", new HashSet<ICuenta>());
		saldoEsperado = 10000;
		giroEsperado = 1000;
	}
	
	@Test
	public void testCrearCuentaCorrienteValida() throws Exception {					
		
		cuenta = new CuentaCorriente(idEsperado, clienteEsperado, 
				saldoEsperado, giroEsperado);
		
		assertEquals(idEsperado, cuenta.getId());
		assertEquals(clienteEsperado, cuenta.getCliente());
		assertEquals(saldoEsperado, cuenta.getSaldo());
		assertEquals(giroEsperado, cuenta.giroEnDescubiertoHabilidado());
	}
		
	@Test(expected = MontoNegativoCuentaException.class)
	public void testGiroEnDescubiertoNoDebeSerNegativo() throws Exception {
		
		long giroInvalido = -1000;
				
		cuenta = new CuentaCorriente(idEsperado, clienteEsperado, 
				saldoEsperado, giroInvalido);
	}
	
	@Test(expected = MontoNegativoCuentaException.class)
	public void testNoDeberiaExtraerMontoNegativo() 
			throws Exception {
		
		long montoInvalido = -1000;
		
		cuenta = new CuentaCorriente(idEsperado, clienteEsperado, 
				saldoEsperado, giroEsperado);
		
		cuenta.extraer(montoInvalido);
	}
	
	@Test(expected = MontoNegativoCuentaException.class)
	public void testExtraerTiraErrorConMontoNoDisponible() 
			throws Exception {
		
		long montoAExtraer = saldoEsperado + giroEsperado + 1000;
		
		cuenta = new CuentaCorriente(idEsperado, clienteEsperado, 
				saldoEsperado, giroEsperado);
		
		cuenta.extraer(montoAExtraer);
	}
	
	@Test
	public void testExtraerDebeDevolverTrue() throws Exception {
		long montoAExtraer = 1000;
		long saldoResultanteEsperado = saldoEsperado - montoAExtraer;
		
		cuenta = new CuentaCorriente(idEsperado, clienteEsperado, 
				saldoEsperado, giroEsperado);
		
		boolean pudoExtraer = cuenta.extraer(montoAExtraer);
		
		assertTrue(pudoExtraer);
		assertEquals(saldoResultanteEsperado, cuenta.getSaldo());
	}
	
	@Test
	public void testToString() throws Exception {
		String stringEsperado = idEsperado + " " + clienteEsperado.getNombre() + " " +
			 saldoEsperado + " " + giroEsperado;
		
		cuenta = new CuentaCorriente(idEsperado, clienteEsperado, 
				saldoEsperado, giroEsperado);
		
		assertEquals(stringEsperado, cuenta.toString());
	}
	
	@Test
	public void testEquals() throws Exception {
		cuenta = new CuentaCorriente(idEsperado, clienteEsperado, 
				saldoEsperado, giroEsperado);
		
		CuentaCorriente otraCuenta = new CuentaCorriente(idEsperado, 
				clienteEsperado, saldoEsperado, giroEsperado);
		
		CajaDeAhorro cuentaCorriente = new CajaDeAhorro(idEsperado,
				clienteEsperado, saldoEsperado);
				
		assertEquals(cuenta, otraCuenta);
		assertNotEquals(cuenta, cuentaCorriente);
		assertNotEquals(cuenta, new Object());
		assertNotEquals(cuenta, null);
	}
}
