package franco.ajedrez;

import static org.junit.Assert.*;

import org.junit.Test;

public class CaballoTest {

	@Test
	public void testMoverDevuelveTrue() {
		int x = 4, y = 4;
		IJugador jugador = new Jugador();
		ISquare squareInicio = new Square(x, y);
		IPieza caballo = new Caballo(jugador, squareInicio);
		squareInicio.setPieza(caballo);
		
		ISquare squareDestino = new Square(x + 1, y + 2);
		
		boolean pudoMoverse = caballo.mover(squareDestino);
		
		assertTrue(pudoMoverse);				
		
		squareInicio = new Square(x, y);
		caballo = new Caballo(jugador, squareInicio);
		squareInicio.setPieza(caballo);
		
		squareDestino = new Square(x + 2, y + 1);
		
		pudoMoverse = caballo.mover(squareDestino);
		
		assertTrue(pudoMoverse);
		
		squareInicio = new Square(x, y);
		caballo = new Caballo(jugador, squareInicio);
		squareInicio.setPieza(caballo);
		
		squareDestino = new Square(x - 1, y - 2);
		
		pudoMoverse = caballo.mover(squareDestino);
		
		assertTrue(pudoMoverse);
		
		squareInicio = new Square(x, y);
		caballo = new Caballo(jugador, squareInicio);
		squareInicio.setPieza(caballo);
		
		squareDestino = new Square(x - 2, y - 1);
		
		pudoMoverse = caballo.mover(squareDestino);
		
		assertTrue(pudoMoverse);
	}
}
