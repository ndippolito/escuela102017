package franco.ajedrez;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PeonTest {

	private IPieza peon;
	private IJugador jugador;	
	private ISquare squareInicio;
	private ISquare squareDestino;
	private int x, y;	
	
	@Before
	public void setup() {		
		x = 1; y = 1;		
		jugador = new Jugador();		
		squareInicio = new Square(x, y);
		peon = new Peon(jugador, squareInicio);	
		squareInicio.setPieza(peon);
		squareDestino = null;			
	}
	
	@Test
	public void testMoverAdelanteDevuelveTrue() {
		squareDestino = new Square(x, y + 1);
		
		boolean pudoMoverse = peon.mover(squareDestino);
		
		assertTrue(pudoMoverse);
		assertEquals(squareDestino, peon.obtenerSquare());
		assertNull(squareInicio.obtenerPieza());
		assertNotNull(squareDestino.obtenerPieza());
	}
	
	@Test
	public void testCapturarDevuelveTrue() {				
		IPieza peonEnemigo;		
		squareDestino = new Square(x + 1, y + 1);
		IJugador jugadorEnemigo = new Jugador();
		peonEnemigo = new Peon(jugadorEnemigo, squareDestino);
		squareDestino.setPieza(peonEnemigo);
		
		boolean pudoCapturar = peon.mover(squareDestino);
		
		assertTrue(pudoCapturar);
		assertEquals(squareDestino, peon.obtenerSquare());		
		assertNull(squareInicio.obtenerPieza());		
		assertNotNull(squareDestino);
	}
	
	@Test
	public void testCapturarDevuelveFalse() {
		IPieza peonAliado;		
		squareDestino = new Square(x + 1, y + 1);		
		peonAliado = new Peon(jugador, squareDestino);		
		squareDestino.setPieza(peonAliado);
		
		boolean pudoCapturar = peon.mover(squareDestino);
		
		assertFalse(pudoCapturar);
		assertNotNull(squareInicio.obtenerPieza());		
		assertNotNull(squareDestino);
		assertEquals(squareInicio, peon.obtenerSquare());
		assertEquals(squareDestino, peonAliado.obtenerSquare());		
		
		squareDestino = new Square(x - 1, y - 1);
		IJugador jugadorEnemigo = new Jugador();
		IPieza peonEnemigo = new Peon(jugadorEnemigo, squareDestino);
		squareDestino.setPieza(peonEnemigo);
		
		pudoCapturar = peon.mover(squareDestino);
		
		assertFalse(pudoCapturar);
		assertNotNull(squareInicio.obtenerPieza());		
		assertNotNull(squareDestino);;	
		assertEquals(squareInicio, peon.obtenerSquare());
		assertEquals(squareDestino, peonEnemigo.obtenerSquare());			
	}
	
	// falta verificar posiciones luego de mover las piezas
	@Test
	public void testMoverDevuelveFalse() {
		squareDestino = new Square(x, y + 2);		
		
		boolean pudoMoverse = peon.mover(squareDestino);
		
		assertFalse(pudoMoverse);		
		
		squareDestino = new Square(x + 1, y);
		
		pudoMoverse = peon.mover(squareDestino);
		
		assertFalse(pudoMoverse);
		
		squareDestino = new Square(x, y - 1);
		
		pudoMoverse = peon.mover(squareDestino);
		
		assertFalse(pudoMoverse);
		
		int limite = 7;
		squareInicio = new Square(limite, limite, (IPieza)peon);
		peon = new Peon(jugador, squareInicio); 
		// peon.setSquare(squareInicio);		
		
		squareDestino = new Square(limite, limite + 1);
		
		pudoMoverse = peon.mover(squareDestino);
		
		assertFalse(pudoMoverse);
		
		squareDestino = new Square(limite + 1, limite);
		
		pudoMoverse = peon.mover(squareDestino);
		
		assertFalse(pudoMoverse);
	}
}
