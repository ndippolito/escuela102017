package franco;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import dipi.ICliente;
import dipi.ICuenta;

public class ClienteTest {
	
	private ICliente cliente;	

	private String nombreEsperado;
	private Set<ICuenta> cuentasEsperadas;
		
	@Before
    public void setup() {  
		cliente = null;		
        nombreEsperado = "Hernan";
        cuentasEsperadas = new HashSet<ICuenta>();
    }
	
	@Test
	public void testCrearClienteValido() throws Exception {
		cliente = new Cliente(nombreEsperado, cuentasEsperadas);
		
		assertEquals(nombreEsperado, cliente.getNombre());
		assertArrayEquals(cuentasEsperadas.toArray(), 
				cliente.getCuentas().toArray());
	}
	
	@Test(expected = NombreInvalidoException.class)	
	public void testCrearClienteNombreInvalidoDebeTirarError() throws Exception {
		String nombreInvalido = "hernan94";		
		
		cliente = new Cliente(nombreInvalido, null);
	}
	
	@Test(expected = NombreInvalidoException.class)
	public void testCrearClienteNombreNuloDebeTirarError() throws Exception {
		cliente = new Cliente(null, null);
	}
	
	@Test
	public void testCrearClienteConCuentasNulas() throws Exception {
		cliente = new Cliente(nombreEsperado, null);
		
		assertNotNull(cliente.getCuentas());
	}
	
	@Test
	public void testAgregarCuentaDeOtroCliente() throws Exception {
		int cantidadCuentasEsperadas = 1;
		
		cliente = new Cliente(nombreEsperado, null);
		
		String otroNombre = "OtroNombre";
		ICliente otroCliente = new Cliente(otroNombre, null);
		
		ICuenta cuenta = new CajaDeAhorro(1, cliente, 10000);
		
		cliente.agregarCuenta(cuenta);
		otroCliente.agregarCuenta(cuenta);
		
		assertEquals(cantidadCuentasEsperadas, cliente.getCuentas().size());
		
		cantidadCuentasEsperadas = 0;
		
		assertEquals(cantidadCuentasEsperadas, otroCliente.getCuentas().size());
	}
	
	@Test
	public void testAgregarCuentaDuplicadas() throws Exception {
		int cantidadCuentasEsperada = 2;
		
		cliente = new Cliente(nombreEsperado, null);
				
		ICuenta cuenta1 = new CajaDeAhorro(1, cliente, 10000);
		ICuenta cuenta2 = new CuentaCorriente(2, cliente, 10000, 1000);
		ICuenta cuenta3 = new CuentaCorriente(2, cliente, 10000, 1000);
		cuentasEsperadas.add(cuenta1);
		cuentasEsperadas.add(cuenta2);		
		
		cliente.agregarCuenta(cuenta1);
		cliente.agregarCuenta(cuenta1);
		cliente.agregarCuenta(cuenta2);
		cliente.agregarCuenta(cuenta2);
		cliente.agregarCuenta(cuenta3);
		cliente.agregarCuenta(cuenta3);
		
		assertEquals(cantidadCuentasEsperada, cliente.getCuentas().size());
		
		for(ICuenta cuentaCliente : cliente.getCuentas()) {
			for(ICuenta cuentaEsperada : cuentasEsperadas) {
				if(cuentaEsperada.equals(cuentaCliente)) {
					cuentasEsperadas.remove(cuentaCliente);
					break;
				}
			}
		}
		
		assertEquals(0, cuentasEsperadas.size());
	}
	
	@Test
	public void testToString() throws Exception {
		String stringEsperado = nombreEsperado + " " + cuentasEsperadas.size();
		
		cliente = new Cliente(nombreEsperado, cuentasEsperadas);
		
		assertEquals(stringEsperado, cliente.toString());
	}
	
	@Test
	public void testEquals() throws Exception {
		cliente = new Cliente(nombreEsperado, cuentasEsperadas);
		ICliente otroCliente = new Cliente(nombreEsperado, 
				cuentasEsperadas);
				
		assertEquals(cliente, cliente);
		assertNotEquals(cliente, otroCliente);
		assertNotEquals(cliente, new Object());		
		assertNotEquals(cliente, null);
	}
	
	/*@Test
	public void testAssertArrayEquals() throws Exception {
		cliente = new Cliente(nombreEsperado, cuentasEsperadas);
		cuentasEsperadas.add(new CajaDeAhorro(1, cliente, 10000));
		cuentasEsperadas.add(new CajaDeAhorro(2, cliente, 12000));
		
		Set<ICuenta> otrasCuentas = new HashSet<ICuenta>();
		
		otrasCuentas.add(new CajaDeAhorro(1, cliente, 10000));
		otrasCuentas.add(new CajaDeAhorro(2, cliente, 12000));				
		
		assertArrayEquals(cuentasEsperadas.toArray(), otrasCuentas.toArray());
	}*/
}
