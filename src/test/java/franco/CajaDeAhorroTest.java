package franco;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class CajaDeAhorroTest {

	private CajaDeAhorro cuenta;
	
	private long idEsperado = 123;
	private ICliente clienteEsperado; 
	private long saldoEsperado;
	
	@Before
	public void setup() throws Exception {
		cuenta = null;
		idEsperado = 123;
		clienteEsperado = new Cliente("Hernan", new HashSet<ICuenta>());
		saldoEsperado = 10000;		
	}
	
	@Test
	public void testCrearCajaDeAhorroValida() throws Exception {
		
		cuenta = new 
				CajaDeAhorro(idEsperado, clienteEsperado, saldoEsperado);		
		
		assertEquals(idEsperado, cuenta.getId());
		assertEquals(clienteEsperado, cuenta.getCliente());
		assertEquals(saldoEsperado, cuenta.getSaldo());
	}
	
	@Test(expected = IdNegativoException.class)
	public void testCrearCajaDeAhorroConIdINegativoDebeTirarError() 
			throws Exception {
		
		long idInvalido = -100;
		
		cuenta = new CajaDeAhorro(idInvalido, clienteEsperado, saldoEsperado);
	}
	
	@Test(expected = MontoNegativoCuentaException.class)
	public void testCrearCajaDeAhorroConSaldoNegativoDebeTirarError()
		throws Exception {
		
		long saldoInvalido = -1000;
		
		cuenta = new CajaDeAhorro(idEsperado, clienteEsperado, saldoInvalido);
	}
	
	@Test(expected = ClienteNuloException.class)
	public void testCrearCajaDeAhorroConClienteNuloDebeTirarError() throws 
		Exception {
		
		Cliente clienteInvalido = null;
		
		cuenta = new CajaDeAhorro(idEsperado, clienteInvalido, saldoEsperado);
	}
	
	@Test
	public void testDepositarDeberiaDevolverTrue() throws Exception {
		
		long montoADepositar = 1000;
		long saldoRestante = saldoEsperado + montoADepositar; 
		
		cuenta = new CajaDeAhorro(idEsperado, clienteEsperado, saldoEsperado);
		
		assertTrue(cuenta.depositar(montoADepositar));
		assertEquals(saldoRestante, cuenta.getSaldo());
	}
	
	@Test(expected = MontoNegativoCuentaException.class)
	public void testExtraerDebeTirarErrorConMontoNoDisponible()
			throws Exception {
		
		long montoAExtraer = 12000;
		
		cuenta = new CajaDeAhorro(idEsperado, clienteEsperado, 
				saldoEsperado);
		
		cuenta.extraer(montoAExtraer);
	}
	
	@Test(expected = MontoNegativoCuentaException.class)
	public void testExtraerDebeTirarErrorConMontoNegativo() 
			throws Exception {
		
		long montoAExtraerInvalido = -1000;
		
		cuenta = new CajaDeAhorro(idEsperado, clienteEsperado, 
				saldoEsperado);
		
		cuenta.extraer(montoAExtraerInvalido);
	}
	
	@Test
	public void testExtraerDebeDevolverTrue() throws Exception {
		
		long montoAExtraer = 8000;
		long saldoResultanteEsperado = 2000;
		
		cuenta = new CajaDeAhorro(idEsperado, clienteEsperado, saldoEsperado);
		
		boolean resultado = cuenta.extraer(montoAExtraer);
		
		assertTrue(resultado);
		assertEquals(saldoResultanteEsperado, cuenta.getSaldo());
	}
	
	@Test(expected = MontoNegativoCuentaException.class)
	public void testDepositarTiraErrorConSaldoNegativo() throws Exception {
		
		long montoInvalido = -1000;
		
		cuenta = new CajaDeAhorro(idEsperado, clienteEsperado, 
				saldoEsperado);
		
		cuenta.depositar(montoInvalido);		
	}
	
	@Test
	public void testToString() throws Exception {
		
		String stringEsperado = idEsperado + " " + clienteEsperado.getNombre() +
				" "	+ saldoEsperado;
		
		cuenta = new CajaDeAhorro(idEsperado, clienteEsperado, saldoEsperado);
		
		assertEquals(stringEsperado, cuenta.toString());
	}
	
	@Test
	public void testEquals() throws Exception {
		cuenta = new CajaDeAhorro(idEsperado, clienteEsperado, 
				saldoEsperado);
		CajaDeAhorro otraCuenta = new CajaDeAhorro(idEsperado, 
				clienteEsperado, saldoEsperado);
		
		CuentaCorriente cuentaCorriente = new CuentaCorriente(idEsperado,
				clienteEsperado, saldoEsperado, 1000);
				
		assertEquals(cuenta, otraCuenta);
		assertNotEquals(cuenta, cuentaCorriente);
		assertNotEquals(cuenta, new Object());
		assertNotEquals(cuenta, null);
	}
}
