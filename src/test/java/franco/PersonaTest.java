package franco;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PersonaTest {
	
    private Persona persona;
  
    private int dniEsperado;
    private String nombreEsperado;
    private String domicilioEsperado;
  
    private final int enteroVacio = 0;
    private final String stringVacio = "";
      
    @Before
    public void setup() {      
        dniEsperado = 38625569;
        nombreEsperado = "Franco";
        domicilioEsperado = "Santiago del Estero 2700";
        persona = null;
    }  
  
    @Test
    public void testCrearPersonaValida() throws Exception {
    	
        persona = new Persona
            (dniEsperado, nombreEsperado, domicilioEsperado);
      
        assertEquals(dniEsperado, persona.getDni());
        assertEquals(nombreEsperado, persona.getNombre());
        assertEquals(domicilioEsperado, persona.getDomicilio());
    }

    @Test
    public void testCrearPersonaConNombreYDomicilioNulos() 
    		throws Exception {
    	
    	persona = new Persona(dniEsperado, null, null);
    	
    	assertEquals(stringVacio, persona.getNombre());
    	assertEquals(stringVacio, persona.getDomicilio());
    }
    
    @Test(expected = DniNegativoException.class)
    public void testCrearPersonaConDniInvalidoDebeTirarError() throws Exception {
    	
    	int dniInvalido = -100;
    	
    	persona = new Persona(dniInvalido, null, null);
    }
    
    @Test
    public void testSetearPersona() throws Exception {
        persona = new Persona(enteroVacio, null, null);
      
        persona.setDni(dniEsperado);
        persona.setNombre(nombreEsperado);
        persona.setDomicilio(domicilioEsperado);
      
        assertEquals(dniEsperado, persona.getDni());
        assertEquals(nombreEsperado, persona.getNombre());
        assertEquals(domicilioEsperado, persona.getDomicilio());
       
        persona.setNombre(null);
        persona.setDomicilio(null);
       
        assertEquals(stringVacio, persona.getNombre());
        assertEquals(stringVacio, persona.getDomicilio());
        
        persona.setNombre(stringVacio);
    }
    
    @Test(expected = DniNegativoException.class)
    public void testSetearDniDebeTirarError() throws Exception {
    	int dniInvalido = -100;
    	
    	persona = new Persona(dniEsperado, nombreEsperado, domicilioEsperado);
    	
    	persona.setDni(dniInvalido);
    }
  
    /**
     * nombre es invalido si
     * 1. esta vacio
     * 2. tiene caracteres que no sean letras  
     */
    @Test
    public void testNombreSoloDeberiaAceptarLetras() throws Exception {
        String nombreInvalido = "Franco94";      
        persona = new Persona(enteroVacio, nombreInvalido, stringVacio);
      
        assertEquals(stringVacio, persona.getNombre());
      
        persona.setNombre(nombreInvalido);
      
        assertEquals(stringVacio, persona.getNombre());
    }
  
    @Test
    public void testEquals() throws Exception {
        persona = new Persona(dniEsperado, null, null);      
        Persona otraPersona = new Persona(dniEsperado, null, null);
       
        assertTrue(persona.equals(otraPersona));
      
        otraPersona.setDni(enteroVacio);
       
        assertFalse(persona.equals(otraPersona));       
        assertFalse(persona.equals(new Object()));
        assertFalse(persona.equals(null));        
    }  
  
    @Test
    public void testToString() throws Exception {       
        persona = new Persona(dniEsperado, nombreEsperado, domicilioEsperado);
      
        String toStringEsperado = dniEsperado + " " + nombreEsperado + " " +
            domicilioEsperado;
      
        assertEquals(toStringEsperado, persona.toString());
    }
}