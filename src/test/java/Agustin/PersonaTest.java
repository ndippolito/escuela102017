package Agustin;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersonaTest {
	
	int dni = 39256300;
	String domicilio = "Av. Siempreviva";
	String nombre = "Agustin";
	Persona persona = new Persona(dni, domicilio, nombre);
	
	
	

	@Test
	public void testCrearPersona() {
		
		assertEquals(dni, persona.getDni());
		assertEquals(domicilio, persona.getDomicilio());
		assertEquals(nombre, persona.getNombre());
	}
	
	@Test
	public void testEqualsPersona() {
		
		// DNI distinto//
		
		int dni2 = 39256301;
		String domicilio2 = "Av. Siempreviva";
		String nombre2 = "Agustin";
		Persona persona2 = new Persona(dni2, domicilio2, nombre2);		
		assertEquals(false, persona.equals(persona2));		
	
		//Nombre distinto//
		int dni3 = 39256300;
		String domicilio3 = "Av. Siempreviva";
		String nombre3 = "Peterlanguila";
		Persona persona3 = new Persona(dni3, domicilio3, nombre3);		
		assertEquals(false, persona.equals(persona3));	
		
		//Domicilio distinto//
		int dni4 = 39256300;
		String domicilio4 = "Av. Siempreviva 1234";
		String nombre4 = "Agustin";
		Persona persona4 = new Persona(dni4, domicilio4, nombre4);		
		assertEquals(false, persona.equals(persona4));	
	
	}
	
	@Test
	public void toStringPersonaTest() {
		assertEquals("Persona [dni=" + this.dni + ", domicilio=" + this.domicilio + ", nombre=" + this.nombre + "]", persona.toString());
	}
	
}
