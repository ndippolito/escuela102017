package Agustin.AjedrezAgustinTest;

import Agustin.Ajedrez.*;
import org.junit.Test;

import java.util.ArrayList;

public class AjedrezTest {

    @Test
    public void test() {
        ArrayList<Pieza> piezasBlancas = new ArrayList<Pieza>();

        ArrayList<Pieza> piezasNegras = new ArrayList<Pieza>();

        Peon peonBlanco1 = new Peon(1);
        Peon peonBlanco2 = new Peon(2);
        Peon peonBlanco3 = new Peon(3);
        Peon peonBlanco4 = new Peon(4);
        Peon peonBlanco5 = new Peon(5);
        Peon peonBlanco6 = new Peon(6);
        Peon peonBlanco7 = new Peon(7);
        Peon peonBlanco8 = new Peon(8);

        Torre torreIzQuierdaBlanco = new Torre(9);
        Torre torreDerechaBlanco = new Torre(10);

        Caballo caballoIzquierdaBlanco = new Caballo(11);
        Caballo caballoDerechaBlanco = new Caballo(12);

        Alfil alfilIzquierdoBlanco = new Alfil(13);
        Alfil alfilDerechoBlanco = new Alfil(14);

        Reina reinaBlanco = new Reina(15);
        Rey reyBlanco = new Rey(16);

        piezasBlancas.add(peonBlanco1);
        piezasBlancas.add(peonBlanco2);
        piezasBlancas.add(peonBlanco3);
        piezasBlancas.add(peonBlanco4);
        piezasBlancas.add(peonBlanco5);
        piezasBlancas.add(peonBlanco6);
        piezasBlancas.add(peonBlanco7);
        piezasBlancas.add(peonBlanco8);
        piezasBlancas.add(torreDerechaBlanco);
        piezasBlancas.add(torreDerechaBlanco);
        piezasBlancas.add(caballoIzquierdaBlanco);
        piezasBlancas.add(caballoDerechaBlanco);
        piezasBlancas.add(alfilIzquierdoBlanco);
        piezasBlancas.add(alfilDerechoBlanco);
        piezasBlancas.add(reinaBlanco);
        piezasBlancas.add(reyBlanco);


        Tablero tablero = new Tablero();
        tablero.colocarPiezas(piezasBlancas, piezasNegras);

        Jugador jugadorBlanco = new Jugador(tablero, piezasBlancas);
        Jugador jugadorNegro = new Jugador(tablero, piezasNegras);

        peonBlanco1.getPosicion();

        jugadorBlanco.moverPiezaA(tablero, peonBlanco1, 2, 6);


        peonBlanco1.getPosicion();

        // FALTA IMPLEMENTAR EL METODO comer() QUE ES LA OTRA ACCION POSIBLE

    }

}
