package Agustin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import Agustin.Cliente;
import Agustin.Cuenta;
import Agustin.CajaDeAhorro;
import org.junit.Test;

public class ClienteTest {

	Cliente cliente = new Cliente("Agustin");

	@Test
	public void testNombreClienteInvalido() {
		Cliente cliente = new Cliente("Agustin123");
		String nombre = cliente.getNombre();
		assertEquals(null, nombre);
	}

	@SuppressWarnings("unused")
	@Test
	public void testAgregarCuenta() throws IdNegativoCuentaException {
		Cuenta cuenta = new CajaDeAhorro(123456, cliente, 200);

		assertEquals(1, cliente.getCuentas().size());
	}

	@Test
	public void testIdValido() throws IdNegativoCuentaException {
		Cuenta cuenta1 = new CajaDeAhorro(1, cliente, 200);
		Cuenta cuenta2 = new CajaDeAhorro(2, cliente, 200);
		Cuenta cuenta3 = new CajaDeAhorro(3, cliente, 200);
		Cuenta cuenta4 = new CajaDeAhorro(4, cliente, 200);

		cliente.agregarCuenta(cuenta1);
		cliente.agregarCuenta(cuenta2);
		cliente.agregarCuenta(cuenta3);
		cliente.agregarCuenta(cuenta4);

		assertFalse(cliente.esIdValido(1));
		assertFalse(cliente.esIdValido(2));
		assertFalse(cliente.esIdValido(3));
		assertFalse(cliente.esIdValido(4));
		assertTrue(cliente.esIdValido(5));

	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testEquals() {
		Cliente cliente = new Cliente("Agustin");
		Cliente cliente2 = new Cliente("Agustin");
		Cliente cliente3 = null;
		Banco banco = new Banco("Nombre de un banco");
		Object object = null;

		assertTrue(cliente.equals(cliente2));
		assertFalse(cliente.equals(cliente3));
		assertFalse(cliente.equals(banco));
		assertFalse(cliente.equals(object));

	}
}
