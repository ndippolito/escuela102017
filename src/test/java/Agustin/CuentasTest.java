package Agustin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import dipi.MontoNegativoCuentaException;

public class CuentasTest {

	@Test
	public void testCrearCuenta() throws IdNegativoCuentaException {
		Cliente cliente = new Cliente("Agustin");
		CuentaCorriente cuenta = new CuentaCorriente(12345, cliente, 1500, 1000);
		assertEquals(cliente, cuenta.getCliente());
		assertEquals("Agustin", cuenta.getCliente().getNombre());
	}

	@Test
	public void testMontoNegativo() throws MontoNegativoCuentaException, IdNegativoCuentaException {

		Cliente cliente = new Cliente("Agustin");
		CuentaCorriente cuenta = new CuentaCorriente(12345, cliente, 500, 1000);

		long saldoAnterior = cuenta.getSaldo();
		cuenta.extraer(700);

		assertEquals(saldoAnterior - 700, cuenta.getSaldo());
	}

	@Test
	public void testExtraccionCorrecta() throws MontoNegativoCuentaException, IdNegativoCuentaException {
		Cliente cliente = new Cliente("Agustin");
		CuentaCorriente cuenta = new CuentaCorriente(12345, cliente, 1000, 1000);

		cuenta.extraer(150);
		assertEquals(850, cuenta.getSaldo());
		cuenta.extraer(850);
		assertEquals(0, cuenta.getSaldo());
		assertEquals(true, cuenta.extraer(500));
		assertEquals(-500, cuenta.getSaldo());
	}

	@Test
	public void testDepositoEnPositivo() throws MontoNegativoCuentaException, IdNegativoCuentaException {
		Cliente cliente = new Cliente("Agustin");
		CuentaCorriente cuenta = new CuentaCorriente(12345, cliente, 1500, 1000);
		cuenta.depositar(500);
		assertEquals(2000, cuenta.getSaldo());
	}

	@Test
	public void testDepositoEnNegativo() throws MontoNegativoCuentaException, IdNegativoCuentaException {
		Cliente cliente = new Cliente("Agustin");
		CuentaCorriente cuenta = new CuentaCorriente(12345, cliente, -200, 1000);

		cuenta.depositar(300);
		assertEquals(100, cuenta.getSaldo());
	}

	@Test
	public void testExtraerSobreGiroHabilitado() throws MontoNegativoCuentaException, IdNegativoCuentaException {
		Cliente cliente = new Cliente("Agustin");
		CuentaCorriente cuenta = new CuentaCorriente(12345, cliente, 1500, 1000);
		long saldoAnterior = cuenta.getSaldo();
		cuenta.extraer(1200);
		assertEquals(saldoAnterior - 1200, cuenta.getSaldo());

	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testEquals() throws IdNegativoCuentaException {
		Cliente cliente = new Cliente("Agustin");
		CuentaCorriente cuenta = new CuentaCorriente(1234, cliente, 100, 500);
		CuentaCorriente cuenta2 = new CuentaCorriente(1234, cliente, 100, 500);
		CuentaCorriente cuenta3 = new CuentaCorriente(1234, cliente, 200, 500);
		Cliente cliente2 = null;
		Banco banco = new Banco("Nombre de un banco");
		Object object = null;

		assertTrue(cuenta.equals(cuenta2));
		assertFalse(cuenta.equals(cuenta3));
		assertFalse(cuenta.equals(cliente2));
		assertFalse(cuenta.equals(banco));
		assertFalse(cuenta.equals(object));

	}

	// @Test
	// public void testCuentasConIdsIguales(){
	// CajaDeAhorro cajaDeAhorro = new CajaDeAhorro(12345, cliente, 500);
	// cliente.agregarCuenta(cajaDeAhorro);
	// cliente.agregarCuenta(cuenta);
	//
	// assertEquals(true, cliente.getCuentas().);
	// }
}
