package Agustin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BancoTest {

	Banco banco = new Banco("Frances");
	Cliente cliente = new Cliente("Agustin");

	@Test
	public void testNombreBancoInvalido() {
		Banco banco = new Banco("123");
		assertEquals(null, banco.getNombre());
	}

	@Test
	public void testNombreBancoValido() {
		assertEquals("Frances", banco.getNombre());
	}

	@Test
	public void testAgregarCliente() {
		Cliente cliente2 = new Cliente("Juan Perez");
		banco.agregarCliente(cliente);
		banco.agregarCliente(cliente2);

		assertEquals(2, banco.getClientes().size());
	}

	@SuppressWarnings("unused")
	@Test
	public void testGetCuentas() throws IdNegativoCuentaException {
		CajaDeAhorro cuenta1 = new CajaDeAhorro(123, cliente, 500);
		CuentaCorriente cuenta2 = new CuentaCorriente(345, cliente, 200, 1000);
		banco.agregarCliente(cliente);

		assertEquals(2, banco.getCuentas().size());
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testEquals() {
		Cliente cliente = new Cliente("Agustin");
		Cliente cliente2 = null;
		Banco banco = new Banco("Nombre de un banco");
		Banco banco2 = new Banco("Nombre de un banco");
		Banco banco3 = new Banco("Nombre de otro banco");

		Object object = null;

		assertTrue(banco.equals(banco2));
		assertFalse(banco.equals(banco3));
		assertFalse(banco.equals(cliente));
		assertFalse(banco.equals(cliente2));
		assertFalse(banco.equals(object));

	}

}
