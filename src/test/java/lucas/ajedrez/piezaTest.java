package lucas.ajedrez;

import static org.junit.Assert.*;

import org.junit.Test;

public class piezaTest {
	
	@Test
	public void crearPieza() {
		Pair<Integer,Integer> posicion = new Pair<Integer,Integer>(0,0);
		Pieza uno = new Pieza("Peon");
	}
	@Test
	public void moverPieza() throws movimientoIncorrectoExceptions {
		Pair<Integer,Integer> movimiento = new Pair<Integer,Integer>(1,0);
		Pair<Integer,Integer> movimiento2 = new Pair<Integer,Integer>(5,5);
		Pair<Integer,Integer> movimiento3 = new Pair<Integer,Integer>(1,-1);
		Pair<Integer,Integer> posicion = new Pair<Integer,Integer>(0,0);
		Pieza uno = new Pieza("Peon");
		Pieza dos = new Pieza("Alfil");
		Pieza tres = new Pieza("Rey");
		uno.establecerPos(posicion);
		dos.establecerPos(posicion);
		tres.establecerPos(posicion);
		assertEquals(0,uno.getPosicion().getPrim());
		assertEquals(0,uno.getPosicion().getSeg());
		uno.mover(movimiento);
		assertEquals(1,uno.getPosicion().getPrim());
		assertEquals(0,uno.getPosicion().getSeg());
		uno.mover(movimiento);
		uno.mover(movimiento);
		uno.mover(movimiento);
		uno.mover(movimiento);
		uno.mover(movimiento);
		uno.mover(movimiento);
		uno.mover(movimiento);
		assertEquals(8,uno.getPosicion().getPrim());
		dos.mover(movimiento2);
		tres.mover(movimiento3);
		assertEquals(5,dos.getPosicion().getPrim());
		assertEquals(5,dos.getPosicion().getSeg());
		assertEquals(1,tres.getPosicion().getPrim());
		assertEquals(-1,tres.getPosicion().getSeg());
	}
		
}
