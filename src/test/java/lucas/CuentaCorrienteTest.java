package lucas;

import org.junit.Test;
import static org.junit.Assert.*;

import dipi.MontoNegativoCuentaException;

public class CuentaCorrienteTest {

	@Test
	public void testConstructor() throws idNegativoExceptions, idRepetidoExceptions {
		Cliente cliente = new Cliente("roman");
		CuentaCorriente uno = new CuentaCorriente((long) 1234, cliente,(long) 200,(long) 1100);
		Cliente cliente2 = new Cliente("pipa");
		CuentaCorriente dos = new CuentaCorriente((long) 504, cliente2,(long) -122,(long) 0);
		assertEquals(1234,uno.getId());
		assertEquals(200,uno.getSaldo());
		assertEquals(1100,uno.giroEnDescubiertoHabilidado());
		assertEquals(true,uno.getCliente().getNombre().equals(cliente.getNombre()));
		assertEquals(504,dos.getId());
		assertEquals(0,dos.getSaldo());
		assertEquals(1000,dos.giroEnDescubiertoHabilidado());
		assertEquals(true,dos.getCliente().getNombre().equals(cliente2.getNombre()));
		
		System.out.println(uno);
		System.out.println(dos);
	}
	
	@Test
	public void testEquals() throws idNegativoExceptions, idRepetidoExceptions {
		Cliente cliente = new Cliente("roman");
		CuentaCorriente uno = new CuentaCorriente((long) 1234, cliente,(long) 200,(long) 100);
		Cliente cliente2 = new Cliente("pipa");
		CuentaCorriente dos = new CuentaCorriente((long) 504, cliente2,(long) -122,(long) 0);
		assertEquals(false, uno.equals(dos));
		Cliente cliente3 = new Cliente("pipa");
		CuentaCorriente tres = new CuentaCorriente((long) 504, cliente3,(long) -122,(long) 0);
		assertEquals(true, dos.equals(tres));
	}
	
	@Test
	public void testDepositarExtraer() throws MontoNegativoCuentaException, idNegativoExceptions, idRepetidoExceptions {
		Cliente cliente = new Cliente("roman");
		CuentaCorriente uno = new CuentaCorriente((long) 1234, cliente,(long) 200,(long) 1100);
		if(uno.depositar(100)){
			assertEquals(300,uno.getSaldo());
		}
		if(uno.extraer(100)){
			assertEquals(200,uno.getSaldo());
		}
		if(uno.extraer(400)){
			assertEquals(-200,uno.getSaldo());
			assertEquals(1100,uno.giroEnDescubiertoHabilidado());
		}
		if(uno.extraer(480)){
			assertEquals(-680,uno.getSaldo());
		}
		//if(uno.extraer(800)){
			//assertEquals(1100,uno.giroEnDescubiertoHabilidado());
		//}
		if(uno.extraer(420)){
			assertEquals(-1100,uno.getSaldo());
			assertEquals(1100,uno.giroEnDescubiertoHabilidado());
		}
		if(uno.depositar(1500)){
			assertEquals(400,uno.getSaldo());
			assertEquals(1100,uno.giroEnDescubiertoHabilidado());
		}
	}
	
}
