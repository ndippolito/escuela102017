package lucas;

import org.junit.Test;
import static org.junit.Assert.*;

public class PersonaTest {
	
	@Test
	public void test() {
		Persona roman = new Persona("roman",(long) 12345678,"calle 123");		
		assertEquals("roman", roman.getNombre());
		assertEquals(12345678, roman.getDni());
		assertEquals("calle 123", roman.getDomicilio());		

		System.out.println(roman.toString());

		roman.setDni((long) 59);
		assertEquals(59, roman.getDni());
		roman.setNombre("manu");
		assertEquals("manu",roman.getNombre());
		roman.setNombre("1234");
		assertEquals("",roman.getNombre());

		System.out.println(roman.toString());

		Persona bou = new Persona((long) 123);
		assertEquals("", bou.getNombre());
		assertEquals(123, bou.getDni());
		assertEquals("", bou.getDomicilio());

		System.out.println(bou.toString());

		bou.setDni((long) 60);
		assertEquals(60,bou.getDni());
		bou.setNombre("manu");
		assertEquals("manu",bou.getNombre());

		System.out.println(bou.toString());

		assertEquals(false, roman.equals(bou));

		System.out.println(roman.equals(bou));		
		
	}
	
}
