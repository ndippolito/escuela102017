package lucas;

import org.junit.Test;
import static org.junit.Assert.*;

public class BancoTest {
	

	@Test
	public void testConstructor() {
		Banco nuevo = new Banco("epi");
		assertEquals("epi",nuevo.getNombre());
		assertEquals(0,nuevo.getClientes().size());
		assertEquals(0,nuevo.getCuentas().size());
		
		System.out.println(nuevo);
	}
	
	@Test
	public void testAgregarCliente() throws idNegativoExceptions, idRepetidoExceptions {
		Banco nuevo = new Banco("epi");
		Cliente cliente = new Cliente("roman");
		Cuenta cuenta = new Cuenta((long) 12345, cliente ,(long) 500);
		cliente.agregarCuenta(cuenta);
		nuevo.agregarCliente(cliente);
		assertEquals(1,nuevo.getClientes().size());
		assertEquals(1,nuevo.getCuentas().size());		
	}
	
	@Test
	public void testEquals() throws idNegativoExceptions, idRepetidoExceptions {
		Banco nuevo = new Banco("epi");
		Banco segundo = new Banco("epi");
		Cliente cliente2 = new Cliente("roman");
		Cuenta cuenta2 = new Cuenta((long) 12345, cliente2 ,(long) 500);
		assertEquals(true, nuevo.equals(segundo));		
		cliente2.agregarCuenta(cuenta2);
		segundo.agregarCliente(cliente2);
		assertEquals(false, nuevo.equals(segundo));
		nuevo.agregarCliente(cliente2);
		assertEquals(true,nuevo.equals(segundo));
		
		System.out.println(nuevo.toString());
	}
	
}
