package lucas;

import org.junit.Test;
import static org.junit.Assert.*;

import dipi.MontoNegativoCuentaException;

public class CuentaTest {

	@Test
	public void testCrearCuenta() throws idNegativoExceptions, idRepetidoExceptions {
		Cliente uno = new Cliente("roman");
		Cliente dos = new Cliente("123847");
		Cuenta nueva = new Cuenta((long) 12345, uno ,(long) 500);	
		Cuenta nuevados = new Cuenta((long) 98746, dos ,(long) -1234);
		assertEquals("roman",nueva.getCliente().getNombre());
		assertEquals(500,nueva.getSaldo());
		assertEquals(12345,nueva.getId());
		
		System.out.println(nueva.toString());		

		assertEquals("",nuevados.getCliente().getNombre());
		assertEquals(0,nuevados.getSaldo());
		assertEquals(98746,nuevados.getId());
		
		System.out.println(nuevados.toString());
		uno.agregarCuenta(nueva);
		//Cuenta tercera = new Cuenta((long) 12345, uno ,(long) 500);
		//Cuenta tercera = new Cuenta((long) -12345, uno ,(long) 500);
		
	}
	
	@Test
	public void testDepositarYExtraer() throws MontoNegativoCuentaException, idNegativoExceptions, idRepetidoExceptions {
		Cliente dos = new Cliente("123847");
		Cuenta nuevados = new Cuenta((long) 98746, dos ,(long) -1234);
		if(nuevados.depositar(1800)) {
			assertEquals(1800,nuevados.getSaldo());
		}
		
		System.out.println(nuevados.toString());
		
		if(nuevados.extraer(800)) {
			assertEquals(1000,nuevados.getSaldo());
		}
		//if(nuevados.extraer(1100)) {
		//	assertEquals(1000,nuevados.getSaldo());
		//}
		//if(nuevados.depositar(-400)) {
		//	assertEquals(1000,nuevados.getSaldo());
		//}
		
		System.out.println(nuevados.toString());
		
	}
	
	@Test
	public void testEquals() throws idNegativoExceptions, idRepetidoExceptions {
		Cliente uno = new Cliente("roman");
		Cliente dos = new Cliente("123847");
		Cuenta nueva = new Cuenta((long) 12345, uno ,(long) 500);	
		Cuenta nuevados = new Cuenta((long) 98746, dos ,(long) -1234);
		
		assertEquals(false, nueva.equals(nuevados));
		long saldouno = nueva.getSaldo();
		Cliente tres = new Cliente("roman");
		Cuenta nuevatres = new Cuenta((long) 12345, tres ,saldouno);
		assertEquals(true, nueva.equals(nuevatres));
	}

}
