package lucas;

import org.junit.Test;
import static org.junit.Assert.*;

public class ClienteTest {
	
	@Test
	public void testConstructor() {
		Cliente primer = new Cliente("Roman");
		assertEquals("Roman", primer.getNombre());
		assertEquals(0, primer.getCuentas().size());
		
		System.out.println(primer);
		
	}

	@Test
	public void testAgregarCuenta() throws idNegativoExceptions, idRepetidoExceptions {
		Cliente primer = new Cliente("roman");
		Cuenta nueva = new Cuenta((long) 12345, primer ,(long) 500);
		primer.agregarCuenta(nueva);
		assertEquals(1,primer.getCuentas().size());
		
		System.out.println(primer);
	}
	
	@Test
	public void testEquals() throws idNegativoExceptions, idRepetidoExceptions {
		Cliente primer = new Cliente("bou");
		Cliente segundo = new Cliente("roman");
		Cuenta nueva = new Cuenta((long) 12345, primer ,(long) 500);
		
		assertEquals(false,primer.equals(segundo));
		segundo.agregarCuenta(nueva);
		primer.agregarCuenta(nueva);
		assertEquals(false,primer.equals(segundo));
		Cliente tercer = primer;
		assertEquals(true,tercer.equals(primer));
	}
	
}
