package dylan;
import dylan.Persona;
import static org.junit.Assert.*;

import org.junit.Test;

public class PersonaTest {
	
	@Test
	public void testVerificarDniPersona() {
		Persona persona = new Persona(40130198);
		assertEquals(40130198, persona.getDni());
	}
	
	@Test
	public void testVerificarNombrePersona() {
		String nombre = "dylan";
		Persona persona = new Persona(40130198, nombre, "biarritz 2480");
		assertEquals("dylan", persona.getNombre());
	}
	
	@Test
	public void testVerificarNombrePersonaConNumeroEnElConstructor() {
		String nombre = "333";
		Persona persona = new Persona(40130198, nombre, "biarritz 2480");
		assertEquals(null, persona.getNombre());
	}

	
	@Test
	public void testVerificarDomicilioPersona() {
		String domicilio = "biarritz 2480";
		Persona persona = new Persona(40130198, "dylan", domicilio);
		assertEquals("biarritz 2480", persona.getDomicilio());
	}
	
	
}
