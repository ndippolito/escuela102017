package dylan;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import dipi.ICuenta;

public class BancoTest {
	
	@Test
	public void verificarNombreTest() {
		Banco banco = new Banco("HSBC");
		assertEquals("HSBC", banco.getNombre());
	}

}
