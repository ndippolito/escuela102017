package dylan;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.junit.Test;

import ajedrezDylan.Casilla;
import ajedrezDylan.Jugador;
import ajedrezDylan.Peon;
import ajedrezDylan.Pieza;

public class JugadorTest {

	@Test
	public void testCrearJugador() {
		Casilla casilla1 = new Casilla(1, 1);
		Casilla casilla2 = new Casilla(4, 4);
		Peon peon = new Peon(casilla1);
		List<Pieza> piezas = new ArrayList<Pieza>();
		piezas.add(peon);
		Jugador jugador = new Jugador("dylan", true, piezas);
		jugador.moverPieza(peon, casilla2);
		System.out.println(peon);
	}

}
