package dylan;

import static org.junit.Assert.*;

import org.junit.Test;

import ajedrezDylan.Casilla;
import ajedrezDylan.Peon;

public class PeonTest {

	@Test
	public void testAsignarCasillaPeon() {
		Casilla casilla = new Casilla(1, 1);
		Peon peon = new Peon(casilla);
		assertEquals(casilla, peon.getCasilla());
	}

}
