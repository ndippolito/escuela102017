package dylan;

import static org.junit.Assert.*;

import org.junit.Test;

import dipi.MontoNegativoCuentaException;

public class CuentaCorrienteTest {

	@Test
	public void verificarSiSeRealizaExtraccionTest() throws MontoNegativoCuentaException {
		Cliente cliente = new Cliente("dylan", null);
		CuentaCorriente cuenta = new CuentaCorriente(300, cliente, 9999, 50);
		assertEquals(true, cuenta.extraer(350));
	}
	
	@Test
	public void verificarSaldoPostExtraccionTest() throws MontoNegativoCuentaException {
		Cliente cliente = new Cliente("dylan", null);
		CuentaCorriente cuenta = new CuentaCorriente(300, cliente, 9999, 50);
		cuenta.extraer(0);
		assertEquals(0, cuenta.getSaldo());
	}

}
