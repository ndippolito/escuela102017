package dylan;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import dipi.ICuenta;

public class ClienteTest {

	@Test
	public void verificarNombreTest() {
		Cliente cliente = new Cliente("dylan", null);
		assertEquals("dylan", cliente.getNombre());
	}
	
	@Test
	public void agregarCuentasTest() {
		Set<ICuenta> cuentas = new HashSet<ICuenta>();
		Cliente cliente = new Cliente("dylan", cuentas);
		CajaDeAhorro caja1 = new CajaDeAhorro(300, cliente, 89654);
		CajaDeAhorro caja2 = new CajaDeAhorro(500, cliente, 89663);
		cliente.agregarCuenta(caja1);
		cliente.agregarCuenta(caja2);
		System.out.println(cliente.getCuentas());
	}
	
	@Test
	public void cuentasIgualesTest() {
		Set<ICuenta> cuentas = new HashSet<ICuenta>();
		Cliente cliente = new Cliente("dylan", cuentas);
		CajaDeAhorro caja1 = new CajaDeAhorro(500, cliente, 89663);
		CajaDeAhorro caja2 = new CajaDeAhorro(500, cliente, 89663);
		assertEquals(true, caja1.equals(caja2));
	}

}
