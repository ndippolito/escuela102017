package dylan;

import static org.junit.Assert.*;

import org.junit.Test;

import dipi.MontoNegativoCuentaException;

public class CajaDeAhorroTest {

	@Test
	public void verificarSaldoTest() {
		Cliente cliente = new Cliente("dylan", null);
		CajaDeAhorro caja = new CajaDeAhorro(300, cliente, 23697);
		assertEquals(300, caja.getSaldo());
	}
	
	@Test
	public void verificarSiSeRealizaExtraccionTest() throws MontoNegativoCuentaException {
		Cliente cliente = new Cliente("dylan", null);
		CajaDeAhorro caja = new CajaDeAhorro(300, cliente, 23697);
		assertEquals(false, caja.extraer(301));
	}
	
	@Test
	public void verificarSaldoPostExtraccionTest() throws MontoNegativoCuentaException {
		Cliente cliente = new Cliente("dylan", null);
		CajaDeAhorro caja = new CajaDeAhorro(300, cliente, 23697);
		caja.extraer(250);
		assertEquals(50, caja.getSaldo());
	}
	
	@Test
	public void verificarSaldoPostDepositoTest() throws MontoNegativoCuentaException {
		Cliente cliente = new Cliente("dylan", null);
		CajaDeAhorro caja = new CajaDeAhorro(300, cliente, 23697);
		caja.depositar(250);
		assertEquals(550, caja.getSaldo());
	}
}
