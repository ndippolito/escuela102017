package dipi.ejemplos;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import org.junit.Test;

public class EjemplosSuperHeroes {
	@Test
	public void testRecorrerLigaDeLaJusticia() throws Exception {
		Collection liga = new HashSet();
		HinchaDeRiver gallina = new HinchaDeRiver("pepe");
		liga.add(gallina);
		liga.add(new HinchaDeBoca("carlos"));
		
		Iterable it = liga;
		this.hacerAlgo(it);
		
		this.hacerOtracosa(gallina);
	}

	private void hacerOtracosa(HinchaDeRiver gallina) throws IOException {
		gallina.setCantidadDePlumas(22.0);
	}

	private void hacerAlgo(Iterable it) {
		for (Object item : it) {
			System.out.println(item.toString());
		}
		
		Iterator iterator = it.iterator();
		while(iterator.hasNext()) {
			Object item = iterator.next();
			System.out.println(item.toString());
		}
	}
}





