package emanuel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;

import org.junit.Test;

import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class CajaDeAhorroTest {

	@Test
	public void testClase() {
		String nombre = "emanuel";
		long dni = 35094291;

		try {
			Cliente cliente = new Cliente(nombre, dni);
			ICuenta ahorro = new CajaDeAhorro(1, cliente);
			ICuenta corriente = new CuentaCorriente(1, cliente);
			assertNotEquals(corriente, ahorro);
			assertEquals(ahorro, ahorro);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testDepositar() {

		String nombre = "emanuel";
		long dni = 35094291;

		try {
			Cliente cliente = new Cliente(nombre, dni);
			ICuenta ahorro = new CajaDeAhorro(1, cliente);

			assertTrue(ahorro.depositar(100));
			assertFalse(ahorro.depositar(0));
			assertFalse(ahorro.depositar(-100));

		} catch (MontoNegativoCuentaException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void TestExtraer() {

		String nombre = "emanuel";
		long dni = 35094291;
		try {
			Cliente cliente = new Cliente(nombre, dni);
			ICuenta ahorro = new CajaDeAhorro(1, cliente);
			cliente.agregarCuenta(ahorro);

			assertEquals(0, ahorro.getSaldo());
			ahorro.depositar(1000);
			assertTrue(ahorro.extraer(100));
			assertEquals(ahorro.getSaldo(), 900);
			assertFalse(ahorro.extraer(1000));
			assertEquals(ahorro.getSaldo(), 900);
			assertTrue(ahorro.extraer(900));
			assertEquals(ahorro.getSaldo(), 0);

		} catch (MontoNegativoCuentaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
