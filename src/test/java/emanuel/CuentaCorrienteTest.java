package emanuel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class CuentaCorrienteTest {

	@Test
	public void testClase() {
		String nombre = "emanuel";
		long dni = 35094291;

		try {
			Cliente cliente = new Cliente(nombre, dni);
			ICuenta ahorro = new CajaDeAhorro(1, cliente);
			ICuenta corriente = new CuentaCorriente(1, cliente);
			assertNotEquals(corriente, ahorro);
			assertEquals(corriente, corriente);
			assertNotEquals(corriente, null);
			assertNotEquals(corriente, new Object());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testExtraer() {

		String nombre = "emanuel";
		long dni = 35094291;
		try {
			Cliente cliente = new Cliente(nombre, dni);
			ICuenta corriente = new CuentaCorriente(1, cliente);

			assertEquals(0, corriente.getSaldo());
			assertFalse(corriente.extraer(-1));
			assertTrue(corriente.extraer(888));
			assertEquals(-888, corriente.getSaldo());
			assertTrue(corriente.extraer(112));
			assertEquals(-1000, corriente.getSaldo());

			assertFalse(corriente.extraer(1));
			assertEquals(-1000, corriente.getSaldo());

		} catch (MontoNegativoCuentaException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testDepositar() {

		String nombre = "emanuel";
		long dni = 35094291;

		try {
			Cliente cliente = new Cliente(nombre, dni);
			ICuenta corriente = new CuentaCorriente(1, cliente);

			assertTrue(corriente.depositar(100));
			assertFalse(corriente.depositar(0));
			assertFalse(corriente.depositar(-100));

		} catch (MontoNegativoCuentaException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
