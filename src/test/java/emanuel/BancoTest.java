package emanuel;

import static org.junit.Assert.*;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import emanuel.Cliente;
import dipi.ICuenta;

public class BancoTest {

	@Test
	public void testCAgregarCliente() {
		try {
			Banco banco = new Banco("galicia");
			Cliente cliente = new Cliente("emanuel", 35094291);
			ICuenta ahorro = new CajaDeAhorro(1, cliente);
			ICuenta corriente = new CuentaCorriente(1, cliente);

			cliente.agregarCuenta(ahorro);
			cliente.agregarCuenta(corriente);
			banco.agregarCliente(cliente);

			Set<ICuenta> lstCuentas = new HashSet<ICuenta>();
			lstCuentas.add(ahorro);
			assertNotEquals(banco.getCuentas(), lstCuentas);
			lstCuentas.add(corriente);
			assertEquals(banco.getCuentas(), lstCuentas);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testClase() {
		try {
			Banco banco = new Banco("galicia");
			Cliente cliente = new Cliente("emanuel", 35094291);
			assertNotEquals(banco, new Banco("frances"));
			assertNotEquals(banco, null);
			assertNotEquals(banco, new Object());
			assertNotEquals(banco, cliente);
			assertEquals(banco, banco);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
