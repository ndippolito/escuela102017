package emanuel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.*;

import org.junit.Test;

public class ClienteTest {

	@Test
	public void testCrearCliente() {

		String nombre = "emanuel";
		long dni = 35094291;

		try {
			Cliente cliente = new Cliente(nombre, dni);
			assertEquals(nombre, cliente.getNombre());
			assertEquals(dni, cliente.getDni());
			assertEquals("emanuel", cliente.getNombre());
			assertNotEquals(cliente, null);
			assertEquals(cliente, cliente);
			assertNotEquals(cliente, new Object());

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public void agregarCuenta() {

		try {
			Cliente cliente1 = new Cliente("emanuel", 35094291);
			Cliente cliente2 = new Cliente("cristian", 9999);

			CajaDeAhorro ahorro1 = new CajaDeAhorro(1, cliente1);
			CuentaCorriente corriente1 = new CuentaCorriente(2, cliente1);
			cliente1.agregarCuenta(ahorro1);
			cliente2.agregarCuenta(corriente1);

			assertNotEquals(ahorro1, corriente1);
			assertEquals(cliente1.getCuentas(), cliente1.getCuentas());
			assertNotEquals(cliente1.getCuentas(), cliente2.getCuentas());
			cliente1.agregarCuenta(corriente1);
			assertNotEquals(cliente1.getCuentas(), cliente2.getCuentas());

		} catch (Exception e) {

			e.printStackTrace();
		}

	}
}
