package pamela;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

public class TestBanco {

	@Test
	public void TestAgregoCliente() {
		Banco banco = new Banco("Banco Pamela");
		Cliente cliente= new Cliente("Jorge", "Lavalle 2852", 133327768);
		Cliente clienteRepetido= new Cliente("Jorge", "Lavalle 2852", 133327768);
		Cliente otroCliente= new Cliente("Jorge", "Lavalle 2852", 14958752);
		
		CuentaCorriente cuentaUno= new CuentaCorriente(789,cliente,555);
		CajaDeAhorro cuentaDos= new CajaDeAhorro(852,cliente);
		CuentaCorriente cuentaTres= new CuentaCorriente(258,otroCliente,255);
		CajaDeAhorro cuentaCuatro= new CajaDeAhorro(123,otroCliente);
		
		cliente.agregarCuenta(cuentaUno);
		cliente.agregarCuenta(cuentaDos);
		cliente.agregarCuenta(cuentaDos);
		
		assertEquals(2, cliente.getCuentas().size());
		
		otroCliente.agregarCuenta(cuentaTres);
		otroCliente.agregarCuenta(cuentaCuatro);
		
		
		banco.agregarCliente(cliente);
		banco.agregarCliente(clienteRepetido);
		
		assertEquals(1, banco.getClientes().size());
		
		banco.agregarCliente(otroCliente);		
		
		assertEquals(2, banco.getClientes().size());
		
		
	}

}
