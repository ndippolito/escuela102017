package pamela;

import dipi.MontoNegativoCuentaException;
import static org.junit.Assert.*;

import org.junit.Test;

public class TestCuentaCorriente {

	@Test
	public void testExtraer() throws MontoNegativoCuentaException {
		Banco banco = new Banco("Banco Pamela");
		Cliente cliente= new Cliente("Jorge", "Lavalle 2852", 133327768);		
		
		CuentaCorriente cuentaUno= new CuentaCorriente(789,cliente,-100);
		CajaDeAhorro cuentaDos= new CajaDeAhorro(852,cliente);
		
		cliente.agregarCuenta(cuentaUno);
		cliente.agregarCuenta(cuentaDos);
		
		cuentaUno.depositar(100);
		cuentaUno.extraer(150);
		assertEquals(-50, cuentaUno.getSaldo());
		
		System.out.println(cuentaUno.getSaldo());
		
		boolean thrown = false;
		try {
			cuentaUno.extraer(150);
		} catch (MontoNegativoCuentaException e) {
			thrown=true;
		}
		
		assertTrue(thrown);
	}

}
