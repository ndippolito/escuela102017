package pamela;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCliente {
	
	Cliente uncliente = new Cliente("Juan","Lavalle 2852",35957632);
	Cuenta unacuenta= new Cuenta(456,uncliente);
	
	@Test
	public void testIngresoNombre() {

		assertEquals("Juan", uncliente.getNombre());
	}
	
	@Test
	public void testIngresoDireccion() {
		
		assertEquals("Lavalle 2852", uncliente.getDireccion());
	}
	
	@Test
	public void testIngresoDni() {
		
		assertEquals(35957632, uncliente.getDni());
	}
}
