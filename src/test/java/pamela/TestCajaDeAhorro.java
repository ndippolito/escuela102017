package pamela;

import static org.junit.Assert.*;

import org.junit.Test;

import dipi.MontoNegativoCuentaException;

public class TestCajaDeAhorro {

	@Test
	public void testEqualsCajas() {
		Banco banco = new Banco("Banco Pamela");
		Cliente cliente= new Cliente("Jorge", "Lavalle 2852", 133327768);		
		
		CajaDeAhorro cuentaUno= new CajaDeAhorro(789,cliente);
		CajaDeAhorro cuentaDos= new CajaDeAhorro(852,cliente);
		
		cliente.agregarCuenta(cuentaUno);
		cliente.agregarCuenta(cuentaDos);
		
		assertFalse(cuentaUno.equals(cuentaDos));
		assertTrue(cuentaUno.equals(cuentaUno));
	}
	
	@Test
	public void testExtraer() throws MontoNegativoCuentaException {
		Banco banco = new Banco("Banco Pamela");
		Cliente cliente= new Cliente("Jorge", "Lavalle 2852", 133327768);		
		
		CajaDeAhorro cajaDeAhorro= new CajaDeAhorro(852,cliente);
		
		cliente.agregarCuenta(cajaDeAhorro);
		
		cajaDeAhorro.depositar(100);
		assertEquals(100, cajaDeAhorro.getSaldo());
		
		cajaDeAhorro.depositar(100);
		assertEquals(200, cajaDeAhorro.getSaldo());
		
		boolean thrown = false;
		try {
			cajaDeAhorro.extraer(300);
		} catch (MontoNegativoCuentaException e) {
			thrown=true;
		}
		
		assertTrue(thrown);
		
		
		boolean thrownf = false;
		try {
			cajaDeAhorro.extraer(200);
		} catch (MontoNegativoCuentaException e) {
			thrownf=true;
		}
		
		assertFalse(thrownf);
	}


}
