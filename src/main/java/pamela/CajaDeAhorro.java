package pamela;

import dipi.MontoNegativoCuentaException;

public class CajaDeAhorro extends Cuenta {

	public CajaDeAhorro(long id_cuenta, Cliente cliente) {
		super(id_cuenta, cliente);
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return super.toString();
	}	
	
	
	@Override
	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {
		if( (this.getSaldo()-montoAExtraer) >= 0) {
			return super.extraer(montoAExtraer);
		}
		else {
			throw new MontoNegativoCuentaException();
		}
	}
	
}
