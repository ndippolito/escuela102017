package pamela;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import dipi.IBanco;
import dipi.ICliente;
import dipi.ICuenta;

public class Banco implements IBanco{

	private String nombre;
	private Set<ICuenta> cuentas;
	private Set<ICliente> clientes;
	
	public Banco(String nombre) {
		this.nombre=nombre;
		this.cuentas=new HashSet<ICuenta>();
		this.clientes=new HashSet<ICliente>();
	}
	
	
	public void agregarCliente(ICliente cliente) {
		if( !this.verificarExistenciaCliente(cliente) ) {
			this.clientes.add(cliente);
		}
	}

	public Set<ICuenta> getCuentas() {
		
		for (Iterator i = this.getClientes().iterator(); i.hasNext();) {
			Cliente cliente = (Cliente) i.next();
			for (Iterator j = cliente.getCuentas().iterator(); j.hasNext();) {
				Cuenta cuenta = (Cuenta) j.next();
				
				this.cuentas.add(cuenta);				
			}			
		}
		
		return this.cuentas;
	}

	public Set<ICliente> getClientes() {
		return this.clientes;
	}

	public String getNombre() {
		return this.nombre;
	}

	@Override
	public String toString() {
		StringBuffer sf =  new StringBuffer();
		sf.append("Nombre del Banco: "+this.nombre);
		sf.append("\n");
		
		return sf.toString();
	}

	public boolean verificarExistenciaCliente(ICliente nuevoCliente) {
		
		boolean existe=false;
		Cliente clienteIngresado= (Cliente)nuevoCliente;
		
		for (Iterator i = this.getClientes().iterator(); i.hasNext();) {
			Cliente unCliente = (Cliente) i.next();
			if(unCliente.getDni()==clienteIngresado.getDni())
			{
				existe=true;
				break;
			}
		}
		
		return existe;
	}

}
