package pamela;
import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class Cuenta implements ICuenta {
	private long idCuenta;
	private Cliente cliente;
	private long saldo;
	
	public Cuenta(long idCuenta, Cliente cliente) {
		this.idCuenta = idCuenta;
		this.cliente = cliente;
		this.saldo = 0;
	}

	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof Cuenta && this instanceof Cuenta) {
			Cuenta unaCuenta = (Cuenta) obj;
			if(this.idCuenta == unaCuenta.idCuenta) {
				return true;
			}
		}		
		return false;
	}

	@Override
	public String toString() {
		
		StringBuffer sf =  new StringBuffer();
		sf.append("Numero de cuenta: "+this.idCuenta);
		sf.append("\n");
		sf.append("Datos del cliente"+this.cliente.toString());
		sf.append("\n");
		sf.append("Saldo Disponible: "+this.saldo);
		return sf.toString();
	}

	public long getId() {
		return this.idCuenta;
	}

	public ICliente getCliente() {
		return this.cliente;
	}

	public long getSaldo() {
		return this.saldo;
	}

	public boolean depositar(long montoADepositar) throws MontoNegativoCuentaException {
		this.saldo+=montoADepositar;
		return true;
	}

	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {
		this.saldo-=montoAExtraer;
		return true;
	}

	
	
}
