package pamela;

import dipi.ICuentaCorriente;
import dipi.MontoNegativoCuentaException;

public class CuentaCorriente extends Cuenta implements ICuentaCorriente{

	private long giroEnDescubierto;

	public CuentaCorriente(long id_cuenta, Cliente cliente, long giroEnDescubierto) {
		super(id_cuenta, cliente);
		this.giroEnDescubierto = giroEnDescubierto;
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return super.toString();
	}

	public long giroEnDescubiertoHabilidado() {
		return this.giroEnDescubierto;
	}

	@Override
	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {
		if( (this.getSaldo()-montoAExtraer) >= this.giroEnDescubierto) {
			return super.extraer(montoAExtraer);
		}
		else {
			throw new MontoNegativoCuentaException();
		}	
	}
}
