package pamela;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import dipi.ICliente;
import dipi.ICuenta;

public class Cliente implements ICliente{
	
	private String nombre;	
	private String direccion;
	private long dni;
	private Set<ICuenta> cuentas;
	
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public long getDni() {
		return this.dni;
	}

	public void setDni(long dni) {
		this.dni = dni;
	}
	
	public Cliente(String nombre, String direccion, long dni) {
		this.nombre=nombre;
		this.direccion=direccion;
		this.dni=dni;
		this.cuentas=new HashSet<ICuenta>();
	}
	
	public void agregarCuenta(ICuenta cuenta) {
		if( !this.verificarExistenciaCuenta(cuenta) ) {
			this.cuentas.add(cuenta);
		}
	}

	public Set<ICuenta> getCuentas() {
		return this.cuentas;
	}	
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj.getClass().toString().equals(Cliente.class.toString()) && this.getClass().toString().equals(Cliente.class.toString())) {
			Cliente otroCliente = (Cliente) obj;
			if(this.dni == otroCliente.dni) {
				return true;
			}
		}		
		return false;
	}

	@Override
	public String toString() {
		
		StringBuffer sf =  new StringBuffer();
		sf.append("\n");
		sf.append("-Nombre: "+this.nombre);
		sf.append("\n");
		sf.append("-Direccion: "+this.direccion);
		sf.append("\n");
		sf.append("-DNI: "+this.dni);
		
		return sf.toString();
	}
	
	public boolean verificarExistenciaCuenta(ICuenta nuevaCuenta) {
			
			boolean existe=false;
			Cuenta cuentaIgresada= (Cuenta)nuevaCuenta;
			
			for (Iterator i = this.getCuentas().iterator(); i.hasNext();) {
				Cuenta unaCuenta = (Cuenta) i.next();
				if(unaCuenta.getId()==cuentaIgresada.getId())
				{
					existe=true;
					break;
				}
			}
			
			return existe;
		}
	
}
