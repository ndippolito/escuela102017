package sam;

import sam.Cliente;
import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class CajaDeAhorro implements ICuenta {
	
	private long idCuenta;
	private long saldo;
	private Cliente cliente;
	
	public CajaDeAhorro(long unIdCuenta, Cliente unCliente, long unSaldo) {		

		while (true) {
			if (saldo < 0) {
				System.out.println("El saldo de la caja de ahorro no puede ser negativo");
			break;
			}				      
		}
		
		this.idCuenta = unIdCuenta;
		this.cliente = unCliente;
		this.saldo = unSaldo;		
	}
	
	public long getId() {
		return this.idCuenta; 
	}
	
	public ICliente getCliente() {
		return this.cliente;
	}
	
	public long getSaldo() {
		return this.saldo;
	}

	public boolean depositar(long montoADepositar)
		throws MontoNegativoCuentaException {
		this.saldo=  saldo + montoADepositar;
		return false;
	}

	public boolean extraer(long montoAExtraer)
		throws MontoNegativoCuentaException {		
		this.saldo = saldo-(montoAExtraer);
		throw new  UnsupportedOperationException();
	}

}
