package sam;

import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class Cuenta implements ICuenta {

	private long saldo;
	private ICliente cliente;
	private long idCuenta;

	public long getId() {
		return this.idCuenta;

	}

	public ICliente getCliente() {
		return this.cliente;

	}

	public long getSaldo() {
		return this.saldo;

	}

	public boolean depositar(long montoADepositar)
		throws MontoNegativoCuentaException {
		throw new  UnsupportedOperationException(); 
	}

	public boolean extraer(long montoAExtraer)
		throws MontoNegativoCuentaException {
		throw new  UnsupportedOperationException(); 
	}

}
