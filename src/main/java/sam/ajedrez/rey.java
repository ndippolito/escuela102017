package sam.ajedrez;

public class rey extends Piezas {
	public rey(boolean disponible, int x, int y) {
    super(disponible, x, y);
	}
	public boolean movValido(Tablero tablero, int fromX, int fromY, int toX, int toY) {
        if(super.movValido(tablero, fromX, fromY, toX, toY) == false)
            return false;
        if(Math.sqrt(Math.pow(Math.abs((toX - fromX)),2)) + Math.pow(Math.abs((toY - fromY)), 2) != Math.sqrt(2)){
            return false;
        }
    return false;
    }


}
