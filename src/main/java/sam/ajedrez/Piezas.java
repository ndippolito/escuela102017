package sam.ajedrez;

public class Piezas implements IPiezas {
	
	private boolean disponible;
	private int x;
	private int y;	   	
	

	public Piezas(boolean disponible2, int x2, int y2) {
		// TODO Auto-generated constructor stub
	}

	public void Pieza(boolean disponible, int x, int y) {
	this.setDisponible(disponible);
	this.setX(x);
	this.setY(y);
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean movValido(Tablero tablero, int fromX, int fromY, int toX,
			int toY) {
		return false;
	}

}
