package sam.ajedrez;

import java.util.ArrayList;
import java.util.List;

public class Jugador {
		public final int peon = 8;
	    public final int alfil = 2;
	    public final int torre = 2;
	    public boolean blanco;

	    private List<Piezas> piezas = new ArrayList<Piezas>();

	    public void jugador(boolean blanco) {	        
	        this.blanco = blanco;
	    }

	    public List<Piezas> getPiezas() {
	        return piezas;
	    }


	    public void initializepiezas(){
	        if(this.blanco == true){
	            for(int i=0; i<peon; i++){ 
		            piezas.add(new peon(true,i,2));		            }
		            piezas.add(new torre(true, 0, 0));
		            piezas.add(new torre(true, 7, 0));
		            piezas.add(new alfil(true, 2, 0));
		            piezas.add(new alfil(true, 5, 0));
		            piezas.add(new caballo(true, 1, 0));
		            piezas.add(new caballo(true, 6, 0));
		            piezas.add(new reina(true, 3, 0));
		            piezas.add(new rey(true, 4, 0));
	        }
	        else{
	            for(int i=0; i<peon; i++){ 
		            piezas.add(new peon(true,i,6));
		            }
		            piezas.add(new torre(true, 0, 7));
		            piezas.add(new torre(true, 7, 7));
		            piezas.add(new alfil(true, 2, 7));
		            piezas.add(new alfil(true, 5, 7));
		            piezas.add(new caballo(true, 1, 7));
		            piezas.add(new caballo(true, 6, 7));
		            piezas.add(new reina(true, 3, 7));
		            piezas.add(new rey(true, 4, 7));
		        }

	    }

		public void setColorBlanco() {
			
		}
}
