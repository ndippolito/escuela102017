package sam.ajedrez;

public class torre extends Piezas {

    public torre(boolean disponible, int x, int y) {
        super(disponible, x, y);
    }


    @Override
    public boolean movValido(Tablero tablero, int fromX, int fromY, int toX, int toY) {
        if(super.movValido(tablero, fromX, fromY, toX, toY) == false)
            return false;
        if(toX == fromX)
            return true;
        if(toY == fromY)
            return true;
        return false;
    }
}
