package sam.ajedrez;

public interface IPiezas {

	boolean movValido(Tablero tablero, int fromX, int fromY, int toX, int toY);

}
