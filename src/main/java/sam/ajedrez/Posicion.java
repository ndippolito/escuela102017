package sam.ajedrez;

public class Posicion {

	    int x;
	    int y;
	    Object pieza;

	    public Posicion(int x, int y) {
	        super();
	        this.x = x;
	        this.y = y;
	        pieza = null;
	    }

	    public void ocuparPosicion(Object piezas){
	        if(this.pieza != null)
	        ((Pieza) this.pieza).setDisponible(false);	        
	        this.pieza = piezas;
	    }

	    public boolean estaOcupada() {
	        if(pieza != null)
	        return true;
	        return false;
	    }

	    public Pieza dejarPosicion() {
	        Pieza piezaLiberada = (Pieza) this.pieza;
	        this.pieza = null;
	        return piezaLiberada;
	    }

	}

