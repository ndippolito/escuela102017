package sam.ajedrez;

public class reina extends Piezas {

	public reina(boolean disponible, int x, int y) {
        super(disponible, x, y);
    }

    @Override
    public boolean movValido(Tablero tablero, int fromX, int fromY, int toX, int toY) {
        if(super.movValido(tablero, fromX, fromY, toX, toY) == false)
            return false;
        //diagonal
        if(toX - fromX == toY - fromY)
            return true;
        if(toX == fromX)
            return true;
        if(toY == fromY)
            return true;

        return false;
    }

}
