package sam.ajedrez;

public class caballo extends Piezas {

	  public caballo(boolean disponible, int x, int y) {
	        super(disponible, x, y);
	    }

	    @Override
	    public boolean movValido(Tablero tablero, int fromX, int fromY, int toX, int toY) {
	        if(super.movValido(tablero, fromX, fromY, toX, toY) == false)
	            return false;

	        if(toX != fromX - 1 && toX != fromX + 1 && toX != fromX + 2 && toX != fromX - 2)
	            return false;
	        if(toY != fromY - 2 && toY != fromY + 2 && toY != fromY - 1 && toY != fromY + 1)
	            return false;

	        return true;
	    }

		public void testMovValido(boolean disponible, int x, int y) {
			// TODO Auto-generated method stub
			
		}

}
