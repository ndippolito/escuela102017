package sam.ajedrez;

public class alfil extends Piezas {
	 public alfil(boolean disponible, int x, int y) {
	        super(disponible, x, y);
	    }

	    @Override
	    public boolean movValido(Tablero tablero, int fromX, int fromY, int toX, int toY) {
	        if(super.movValido(tablero, fromX, fromY, toX, toY) == false)
	            return false;

	        if(toX - fromX == toY - fromY)
	            return true;

	        return false;
	    }



}
