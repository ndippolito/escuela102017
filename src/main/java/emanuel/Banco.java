package emanuel;

import java.util.HashSet;
import java.util.Set;

import dipi.IBanco;
import dipi.ICliente;
import dipi.ICuenta;

public class Banco implements IBanco {
	private String nombre;
	private Set<ICliente> clientes = new HashSet<ICliente>();

	public Banco(String nombre) throws Exception {
		super();
		this.setNombre(nombre);

	}

	public void agregarCliente(ICliente cliente) {
		clientes.add(cliente);
	}

	public Set<ICuenta> getCuentas() {
		Set<ICuenta> cuentas = new HashSet<ICuenta>();

		for (ICliente c : getClientes()) {
			cuentas.addAll(c.getCuentas());
		}

		return cuentas;
	}

	public Set<ICliente> getClientes() {
		return clientes;
	}

	public void setNombre(String nombre) throws Exception {
		if (Funciones.esCadenaLetras(nombre)) {
			this.nombre = nombre;
		} else {
			throw new Exception("solo letras");
		}
	}

	public String getNombre() {
		return nombre;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Banco other = (Banco) obj;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Banco [nombre=" + nombre + "]";
	}

}
