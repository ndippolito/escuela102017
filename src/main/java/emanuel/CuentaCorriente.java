package emanuel;

import dipi.ICuentaCorriente;
import dipi.MontoNegativoCuentaException;

public class CuentaCorriente extends CajaDeAhorro implements ICuentaCorriente {
	public CuentaCorriente(long id, Cliente cliente) {
		super(id, cliente);
	}

	@Override
	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {

		boolean extraer = false;
		long saldo = getSaldo();

		if (saldo >= 0 && montoAExtraer > 0 && montoAExtraer<=saldo) {

			saldo -= montoAExtraer;
			setSaldo(saldo);
			extraer = true;
		} else {
			extraer = false;
		}

		if (saldo > giroEnDescubiertoHabilidado() && saldo <= 0 && montoAExtraer>0) {

			saldo = saldo - montoAExtraer;

			if (saldo >= giroEnDescubiertoHabilidado()) {
				extraer = true;
				setSaldo(saldo);
			} else {
				extraer = false;
			}
		}

		return extraer;
	}

	public long giroEnDescubiertoHabilidado() {

		long limite = -1000;
		return limite;
	}

	
}
