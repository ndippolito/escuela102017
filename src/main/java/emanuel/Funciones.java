package emanuel;

public class Funciones {

	public static boolean esCadenaLetras(String string) {
		boolean respuesta = false;
		if ((string).matches("([a-z]|[A-Z]|\\s)+")) {
			respuesta = true;
		}
		return respuesta;
	}
}
