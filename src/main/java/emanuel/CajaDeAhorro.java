package emanuel;

import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class CajaDeAhorro implements ICuenta {
	private long id;
	private long saldo;
	private Cliente cliente;

	public CajaDeAhorro(long id,  Cliente cliente) {
		this.id = id;
		this.saldo = 0;
		this.cliente = cliente;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setSaldo(long saldo) {
		this.saldo = saldo;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	public ICliente getCliente() {
		// TODO Auto-generated method stub
		return cliente;
	}

	public long getSaldo() {
		// TODO Auto-generated method stub
		return saldo;
	}

	public boolean depositar(long montoADepositar) throws MontoNegativoCuentaException {
		boolean depositar = false;
		if (montoADepositar > 0) {
			this.saldo += montoADepositar;
			depositar = true;

		} else {
			depositar = false;
		}
		return depositar;
	}

	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {
		boolean extraer = false;
		if (montoAExtraer <= saldo && saldo > 0 && montoAExtraer > 0) {
			this.saldo -= montoAExtraer;
			extraer = true;
		} else {
			extraer = false;
		}

		return extraer;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CajaDeAhorro other = (CajaDeAhorro) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (id != other.id)
			return false;
		if (saldo != other.saldo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cuenta [id=" + id + ", saldo=" + saldo + ", cliente=" + cliente + "]";
	}

}
