package emanuel.ajedrezEmanuel;

public class Peon extends Pieza {

	public Peon(int idPieza, boolean muerto, Jugador jugador) {
		super(idPieza, muerto, jugador);
	}

	@Override
	public Pieza[][] mover(Tablero tablero, int idPieza) {
		Pieza[][] tableroActual = tablero.getPosiciones();
		ResultadoPiezaXY resultado = tablero.traerPiezaXY(idPieza);

		tableroActual[resultado.getX()][resultado.getY() + 1] = resultado.getPieza();
		tableroActual[resultado.getX()][resultado.getY()] = null;

		return tableroActual;
	}

}
