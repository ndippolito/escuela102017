package emanuel.ajedrezEmanuel;

public class Tablero {
	private Pieza[][] posiciones = new Pieza[8][8];
	private int nroTurno;

	public Tablero() {
		this.setPosiciones();
		this.nroTurno = 0;
	}

	public int getNroTurno() {
		return nroTurno;
	}

	public void setNroTurno(int nroTurno) {
		this.nroTurno = nroTurno;
	}

	public Pieza[][] getPosiciones() {
		return posiciones;
	}

	public void setPosiciones() {

		Jugador jugador1 = new Jugador(1, "Emanuel");
		Jugador jugador2 = new Jugador(1, "Emanuel");

		Pieza peon1 = new Peon(1, false, jugador1);
		Pieza peon2 = new Peon(2, false, jugador1);
		Pieza peon3 = new Peon(3, false, jugador1);
		Pieza peon4 = new Peon(4, false, jugador1);
		Pieza peon5 = new Peon(5, false, jugador1);
		Pieza peon6 = new Peon(6, false, jugador1);
		Pieza peon7 = new Peon(7, false, jugador1);
		Pieza peon8 = new Peon(8, false, jugador1);
		Pieza peon9 = new Peon(9, false, jugador2);
		Pieza peon10 = new Peon(10, false, jugador2);
		Pieza peon11 = new Peon(11, false, jugador2);
		Pieza peon12 = new Peon(12, false, jugador2);
		Pieza peon13 = new Peon(13, false, jugador2);
		Pieza peon14 = new Peon(14, false, jugador2);
		Pieza peon15 = new Peon(15, false, jugador2);
		Pieza peon16 = new Peon(16, false, jugador2);

		Pieza torre1 = new Torre(17, false, jugador1);
		Pieza torre2 = new Torre(18, false, jugador1);
		Pieza torre3 = new Torre(19, false, jugador2);
		Pieza torre4 = new Torre(20, false, jugador2);

		Pieza caballo1 = new Caballo(21, false, jugador1);
		Pieza caballo2 = new Caballo(22, false, jugador1);
		Pieza caballo3 = new Caballo(23, false, jugador2);
		Pieza caballo4 = new Caballo(24, false, jugador2);

		Pieza alfil1 = new Alfil(25, false, jugador1);
		Pieza alfil2 = new Alfil(26, false, jugador1);
		Pieza alfil3 = new Alfil(27, false, jugador2);
		Pieza alfil4 = new Alfil(28, false, jugador2);

		Pieza dama1 = new Dama(29, false, jugador1);
		Pieza dama2 = new Dama(30, false, jugador2);

		Pieza rey1 = new Rey(31, false, jugador1);
		Pieza rey2 = new Rey(32, false, jugador2);

		// ubicando jugador 1
		posiciones[1][0] = peon1;
		posiciones[1][1] = peon2;
		posiciones[1][2] = peon3;
		posiciones[1][3] = peon4;
		posiciones[1][4] = peon5;
		posiciones[1][5] = peon6;
		posiciones[1][6] = peon7;
		posiciones[1][7] = peon8;

		posiciones[0][0] = torre1;
		posiciones[0][7] = torre2;

		posiciones[0][1] = caballo1;
		posiciones[0][6] = caballo2;

		posiciones[0][2] = alfil1;
		posiciones[0][5] = alfil2;

		posiciones[0][3] = dama1;
		posiciones[0][4] = rey1;

		// ubicando jugadores n°2

		posiciones[6][0] = peon9;
		posiciones[6][1] = peon10;
		posiciones[6][2] = peon11;
		posiciones[6][3] = peon12;
		posiciones[6][4] = peon13;
		posiciones[6][5] = peon14;
		posiciones[6][6] = peon15;
		posiciones[6][7] = peon16;

		posiciones[7][0] = torre3;
		posiciones[7][7] = torre4;

		posiciones[7][1] = caballo3;
		posiciones[7][6] = caballo4;

		posiciones[7][2] = alfil3;
		posiciones[7][5] = alfil4;

		posiciones[7][3] = dama2;
		posiciones[7][4] = rey2;

	}

	public void imprimir() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				System.out.println(posiciones[i][j]);

			}
		}
	}

	public emanuel.ajedrezEmanuel.ResultadoPiezaXY traerPiezaXY(int idPieza) {
		emanuel.ajedrezEmanuel.ResultadoPiezaXY resultado = new emanuel.ajedrezEmanuel.ResultadoPiezaXY();
		int i = 0;
		int j = 0;

		for (i = 0; i <= posiciones.length; i++) {
			for (j = 0; j <= posiciones.length; j++) {
				if (posiciones[i][j].getIdPieza() == idPieza) {
					resultado.setPieza(posiciones[i][j]);
					resultado.setX(i);
					resultado.setY(j);

				}
			}
		}
		return resultado;
	}

	public int sumarTurno() {
		return this.nroTurno += 1;
	}
}
