package emanuel.ajedrezEmanuel;

public class ResultadoPiezaXY {
	private Pieza pieza;
	private int x;
	private int y;

	public ResultadoPiezaXY() {

	}

	public Pieza getPieza() {
		return pieza;
	}

	public void setPieza(Pieza pieza) {
		this.pieza = pieza;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
