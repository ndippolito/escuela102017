package emanuel;

import java.util.HashSet;
import java.util.Set;

import dipi.ICliente;
import dipi.ICuenta;

public class Cliente implements ICliente {
	private String nombre;
	private long dni;
	private Set<ICuenta> cuentas = new HashSet<ICuenta>();

	public Cliente(String nombre, long dni) throws Exception {
		super();
		this.setNombre(nombre);
		this.dni = dni;
	}

	public void setCuentas(Set<ICuenta> cuentas) {
		this.cuentas = cuentas;
	}

	public long getDni() {
		return dni;
	}

	public void setDni(long dni) {
		this.dni = dni;
	}

	public void setNombre(String nombre) throws Exception {
		if (Funciones.esCadenaLetras(nombre)) {
			this.nombre = nombre;
		} else {
			throw new Exception("solo letras");
		}

	}

	public void agregarCuenta(ICuenta cuenta) {
		cuentas.add(cuenta);
	}

	public Set<ICuenta> getCuentas() {
		return cuentas;
	}

	public String getNombre() {
		return nombre;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (dni != other.dni)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cliente [nombre=" + nombre + ", dni=" + dni + "]";
	}

}
