package franco;

import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class CajaDeAhorro extends Cuenta {	
		
	public CajaDeAhorro(long id, ICliente cliente, long saldo) 
			throws Exception {
		
		super(id, cliente);
		
		long montoMinimo = 0;

		if(saldo >= montoMinimo) {
			this.saldo = saldo;
		}
		else {
			throw new MontoNegativoCuentaException();
		}
	}
	
	public boolean extraer(long montoAExtraer) 
			throws MontoNegativoCuentaException {
		
		boolean puedeExtraer = false;
		
		if(montoAExtraer >= 0) {		
			if(saldo >= montoAExtraer) {
				puedeExtraer = true;
				saldo -= montoAExtraer;			
			}
			else {
				throw new MontoNegativoCuentaException();				
			}
		}
		else {
			throw new MontoNegativoCuentaException();
		}
		
		return puedeExtraer;		
	}	
}
