package franco;

import dipi.ICliente;
import dipi.ICuentaCorriente;
import dipi.MontoNegativoCuentaException;

public class CuentaCorriente extends Cuenta implements ICuentaCorriente {

	private long giroEnDescubierto;
	
	public CuentaCorriente(long id, ICliente cliente, 
			long saldo, long giroEnDescubierto) throws Exception
	{
		super(id, cliente);
		
		if(giroEnDescubierto >= 0) {
			this.giroEnDescubierto = giroEnDescubierto;
		}
		else {
			throw new MontoNegativoCuentaException();
		}
		
		if(saldo >= (-1 * this.giroEnDescubierto)) {
			this.saldo = saldo;
		}
		else {
			saldo = 0;	
		}
	}
	
	@Override
	public boolean extraer(long montoAExtraer) 
			throws MontoNegativoCuentaException {
		
		boolean puedeExtraer = false;
		
		if(montoAExtraer >= 0) {
			if((saldo - montoAExtraer) >= (-1 * giroEnDescubierto)) {
				puedeExtraer = true;
				saldo -= montoAExtraer;
			}
			else {
				throw new MontoNegativoCuentaException();
			}
		}
		else {
			throw new MontoNegativoCuentaException();
		}
		
		return puedeExtraer;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + giroEnDescubierto;
	}

	public long giroEnDescubiertoHabilidado() {
		return giroEnDescubierto;
	}
}
