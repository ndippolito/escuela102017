package franco;

import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public abstract class Cuenta implements ICuenta {

	private long id;
	private ICliente cliente;
	protected long saldo;
	
	public Cuenta(long id, ICliente cliente) 
			throws Exception {
		
		long montoMinimo = 0;
		
		if(id >= montoMinimo) {
			this.id = id;
		}
		else {
			throw new IdNegativoException();
		}	
		
		if(cliente != null) {
			this.cliente = cliente;	
		}
		else {
			throw new ClienteNuloException();
		}		
	}
	
	public long getId() {
		return id;
	}

	public ICliente getCliente() {
		return cliente;
	}

	public long getSaldo() {
		return saldo;
	}

	public boolean depositar(long montoADepositar) 
			throws MontoNegativoCuentaException {
		
		boolean puedeDepositar = false;
		
		if(montoADepositar >= 0) {
			puedeDepositar = true;
			saldo += montoADepositar;
		}
		else {
			throw new MontoNegativoCuentaException();
		}
		
		return puedeDepositar;
	}

	public abstract boolean extraer(long montoAExtraer) 
			throws MontoNegativoCuentaException;

	@Override
	public boolean equals(Object object) {	
		boolean sonIguales = false;
		
		if(object == null) {
			return sonIguales;
		}
		else if(object.getClass().equals(this.getClass())) {
			ICuenta cuenta = (ICuenta)object;
			
			sonIguales = id == cuenta.getId();
		}		
		
		return sonIguales;
	}
	
	@Override
	public String toString() {		
		ICliente persona = (ICliente)cliente;
		
		return id + " " + persona.getNombre() + " " + saldo;
	}
}