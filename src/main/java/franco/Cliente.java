package franco;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import dipi.ICliente;
import dipi.ICuenta;

public class Cliente implements ICliente {
	
	private String nombre;
	private Set<ICuenta> cuentas;
	
	public Cliente(String nombre, Set<ICuenta> cuentas) throws Exception {   
        if(nombre != null) {      
            if (Pattern.matches("[a-zA-Z]+", nombre)) {
                this.nombre = nombre;
            } else {
                throw new NombreInvalidoException();
            }
        }
        else {
        	throw new NombreInvalidoException();
        } 
         
        if(cuentas != null) {
        	this.cuentas = cuentas;
        }
        else {
        	this.cuentas = new HashSet<ICuenta>();
        }
	}
	
	public void agregarCuenta(ICuenta cuenta) {
		
		boolean puedeAgregar = true;
		
		if(this.equals(cuenta.getCliente())) {
			for(ICuenta c : cuentas) {
				if(c.equals(cuenta)) {
					puedeAgregar = false;
					break;
				}
			}
		}
		else {
			puedeAgregar = false;
		}
		
		if(puedeAgregar) {
			cuentas.add(cuenta);
		}
	}

	public Set<ICuenta> getCuentas() {
		return cuentas;
	}

	public String getNombre() {
		return nombre;
	}
	
	@Override
	public String toString() {
		return nombre + " " + cuentas.size();
	}
	
	@Override
	public boolean equals(Object object) {
		boolean sonIguales = false;
		
		if(object == null) {
			return sonIguales;
		}
		else if(this == object) {
			sonIguales = true;
		}
		
		return sonIguales;
	}
}
