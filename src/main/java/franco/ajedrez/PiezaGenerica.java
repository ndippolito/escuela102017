package franco.ajedrez;

public abstract class PiezaGenerica implements IPieza {

	private IJugador jugador;
	protected ISquare square;
	protected final int limite = 7;	
	
	public PiezaGenerica(IJugador jugador, ISquare square) {
		this.jugador = jugador;
		this.square = square;
	}
	
	public IJugador obtenerJugador() {
		return jugador;
	}

	public abstract boolean mover(ISquare squareDestino);

	public ISquare obtenerSquare() {		
		return square;
	}
	
	protected void capturarPieza(ISquare squareDestino) {
		squareDestino.setPieza(this);
		this.square.setPieza(null);
		this.square = squareDestino;
	}

}
