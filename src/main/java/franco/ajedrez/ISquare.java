package franco.ajedrez;

public interface ISquare {
	public boolean setPieza(IPieza pieza);
	public IPieza obtenerPieza();
	public int obtenerFila();
	public int obtenerColumna();
	public boolean estaVacio();
}
