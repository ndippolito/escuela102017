package franco.ajedrez;

public class Square implements ISquare {

	//private ITablero tablero;
	private IPieza pieza;
	private int x;
	private int y;	
	
	// agregar validacion cuando haga los test
	// no se puede establecer una posicion invalida, tira error
	public Square(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public Square(int x, int y, IPieza pieza) {
		//pieza.setSquare(this);
		this.pieza = pieza;		
		this.x = x;
		this.y = y;
	}
	
	public boolean setPieza(IPieza pieza) {
		this.pieza = pieza;
		return true;
	}

	public IPieza obtenerPieza() {
		return pieza;
	}

	public int obtenerFila() {		
		return y;
	}

	public int obtenerColumna() {
		return x;
	}

	public boolean estaVacio() {
		return pieza == null;
	}
}
