package franco.ajedrez;

import java.util.Set;

public interface ITablero {	
	public Set<ISquare> obtenerSquares();
	public Set<IPieza> obtenerPiezas();
	public boolean setSquare(ISquare square);
	public IPieza obtenerSquare(int x, int y);
}
