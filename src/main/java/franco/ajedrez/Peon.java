package franco.ajedrez;

public class Peon extends PiezaGenerica implements IPieza {

    /*private IJugador jugador;
	private ISquare square;	
	private int limite = 7;*/	
		
	public Peon(IJugador jugador, ISquare square) {
		super(jugador, square);		
	}

	public boolean mover(ISquare squareDestino) {		
		boolean puedeMoverse = false;
		
		if(squareDestino.estaVacio()) {
			if(this.square.obtenerFila() < limite &&
			this.square.obtenerColumna() ==  squareDestino.obtenerColumna() && 
			this.square.obtenerFila() + 1 == squareDestino.obtenerFila()) {
				puedeMoverse = true;
				this.square.setPieza(null);
				squareDestino.setPieza(this);				
				this.square = squareDestino;
			}
		}
		else {
			if(!this.obtenerJugador().equals
					(squareDestino.obtenerPieza().obtenerJugador()) &&
			(this.square.obtenerColumna() +1 == squareDestino.obtenerColumna() || 
			this.square.obtenerColumna() - 1 == squareDestino.obtenerColumna()) &&
			this.square.obtenerFila() + 1 == squareDestino.obtenerFila() ) {
				puedeMoverse = true;
				this.square.setPieza(null);
				this.square = squareDestino;
				squareDestino.setPieza(this);
			}
		}
		
		return puedeMoverse;
	}	

	/*public boolean setSquare(ISquare square) {
		return false;
	}*/
}
