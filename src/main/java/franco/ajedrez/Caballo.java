package franco.ajedrez;

public class Caballo extends PiezaGenerica implements IPieza {

	public Caballo(IJugador jugador, ISquare square) {
		super(jugador, square);
	}

	@Override
	public boolean mover(ISquare squareDestino) {
		boolean puedeMoverse = false;
		
		int thisColumna = this.square.obtenerColumna(), 
				thisFila = this.square.obtenerFila();
		int columnaDestino = squareDestino.obtenerColumna(), 
				filaDestino = squareDestino.obtenerFila() ;
		
		int diferenciaColumna = Math.abs(thisColumna - columnaDestino);
		int diferenciaFila = Math.abs(thisFila - filaDestino);
		
		if(diferenciaColumna == 2 && diferenciaFila == 1) {
			puedeMoverse = true;
		}
		else if(diferenciaColumna == 1 && diferenciaFila == 2) {
			puedeMoverse = true;
			
		}
		
		if(puedeMoverse) {
			if(!squareDestino.estaVacio()) {
				if(!this.square.obtenerPieza().obtenerJugador()
						.equals(squareDestino.obtenerPieza().obtenerJugador())) {
					puedeMoverse = true;
					this.capturarPieza(squareDestino);					
				}
			}
			else {
				puedeMoverse = true;
				this.square.setPieza(null);
				squareDestino.setPieza(this);
			}
		}
		
		return puedeMoverse;
	}
}
