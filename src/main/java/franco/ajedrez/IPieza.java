package franco.ajedrez;

public interface IPieza {	
	public IJugador obtenerJugador();
	public boolean mover(ISquare squareDestino);
	//public boolean setSquare(ISquare square);
	public ISquare obtenerSquare();
}
