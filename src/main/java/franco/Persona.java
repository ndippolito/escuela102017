package franco;

import java.util.regex.Pattern;

public class Persona  {
   
    private int dni;
    private String domicilio;
    private String nombre;
    
    public Persona(int dni, String nombre, String domicilio) throws Exception {
        this.setDni(dni);        
    	this.setNombre(nombre);        
        this.setDomicilio(domicilio);
    }
  
    public int getDni()
    {
        return dni;
    }
    public void setDni(int dni) throws Exception {
    	if(dni >= 0 ) {
        	this.dni = dni;
        }    	
        else {
        	throw new DniNegativoException();
        }
    }
  
    public String getDomicilio() {
        return domicilio;
    }
    public void setDomicilio(String domicilio) {
        if(domicilio != null) {       
            this.domicilio = domicilio;
        }
        else {
            this.domicilio = "";
        }           
    }
  
    public String getNombre() {
        return nombre;
    }  
    public void setNombre(String nombre) {
        String stringVacio = "";
       
        if(nombre != null) {      
            if (Pattern.matches("[a-zA-Z]+", nombre)) {
                this.nombre = nombre;
            } else {
                this.nombre = stringVacio;
            }
        }
        else {
            this.nombre = stringVacio;
        }           
    }
  
    @Override
    public String toString() {
        return (dni + " " + nombre + " " + domicilio);
    }
  
    @Override
    public boolean equals(Object object) {
        boolean sonIguales = false;
       
        if(object == null) {
            sonIguales = false;
        }       
        else if(object instanceof Persona) {
            Persona persona = (Persona)object;
           
            sonIguales = this.dni == persona.getDni();   
        }       
       
        return sonIguales;
    }  
}