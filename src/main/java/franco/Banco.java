package franco;

import java.util.HashSet;
import java.util.Set;

import dipi.IBanco;
import dipi.ICliente;
import dipi.ICuenta;

public class Banco implements IBanco {

	private String nombre;
	private Set<ICliente> clientes;	
	
	public Banco(String nombre, Set<ICliente> clientes) throws Exception {				
		if(nombre != null) {
			this.nombre = nombre;
		}
		else {
			throw new NombreInvalidoException();
		}
		
		if(clientes != null) {
			this.clientes = clientes;
		}
		else {
			this.clientes = new HashSet<ICliente>();
		}		
	}
	
	public void agregarCliente(ICliente cliente) {
		clientes.add(cliente);
	}

	public Set<ICuenta> getCuentas() {
		Set<ICuenta> cuentas = new HashSet<ICuenta>();
		
		for(ICliente cliente : clientes) {
			for(ICuenta cuenta : cliente.getCuentas()) {
				cuentas.add(cuenta);
			}
		}
		
		return cuentas;
	}

	public Set<ICliente> getClientes() {
		return clientes;
	}

	public String getNombre() {
		return nombre;
	}
		
	@Override
	public String toString() {
		return nombre;
	}
	
	@Override
	public boolean equals(Object object) {
		IBanco banco = (IBanco)object;
		
		return this.nombre.equals(banco.getNombre());
	}
}
