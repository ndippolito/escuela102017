package ajedrezEmanuel;

public abstract class Pieza {
	private int idPieza;
	private boolean muerto;
	private Jugador jugador;

	public Pieza(int idPieza, boolean muerto, Jugador jugador) {
		this.idPieza = idPieza;
		this.muerto = false;
		this.jugador = jugador;
	}

	public int getIdJugador() {
		return jugador.getIdJugador();
	}

	public void setIdJugador(int idJugador) {
		jugador.setIdJugador(idJugador);
	}

	public abstract Tablero mover(Tablero tablero, int idPieza, int x, int y);

	public int getIdPieza() {
		return idPieza;
	}

	public void setIdPieza(int idPieza) {
		this.idPieza = idPieza;
	}

	public boolean isMuerto() {
		return muerto;
	}

	public void setMuerto(boolean muerto) {
		this.muerto = muerto;
	}

	@Override
	public String toString() {
		return "Pieza [idPieza=" + idPieza + ", muerto=" + muerto + ", jugador=" + jugador + "]";
	}

}
