package pablo;

public class CajaDeAhorro extends Cuenta {
	
	public CajaDeAhorro(long id, Cliente cliente) {
		super(id, cliente);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(CajaDeAhorro.class)) {
			CajaDeAhorro other = (CajaDeAhorro) obj;
			return (this.id == other.id) &&
					(this.cliente.equals(other.cliente)) &&
					(this.saldo == other.saldo);
		} else {
			return false;
		}
	}

}
