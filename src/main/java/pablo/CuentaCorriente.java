package pablo;

public class CuentaCorriente extends Cuenta implements dipi.ICuentaCorriente{
	
	private long giroEnDescubierto;
	
	public CuentaCorriente(long id, Cliente cliente, long giroEnDescubierto) {
		super(id, cliente);
		this.giroEnDescubierto = giroEnDescubierto;

	}
	
	public long giroEnDescubiertoHabilidado() {
		return giroEnDescubierto;
	}
	
	@Override
	public boolean extraer(long montoAExtraer) throws dipi.MontoNegativoCuentaException {
		boolean seExtrajo;
		if (montoAExtraer <= this.saldo + giroEnDescubierto) {
			this.saldo = this.saldo - montoAExtraer;
			seExtrajo = true;
		}
		else {
			seExtrajo = false;
		}
		return seExtrajo;
	}

	@Override
	public String toString() {
		StringBuffer tmp = new StringBuffer();
		tmp.append("[");
		tmp.append(this.getId());
		tmp.append(", ");
		tmp.append(this.cliente.toString());
		tmp.append(", ");
		tmp.append(this.getSaldo());
		tmp.append("]");
		tmp.append(", ");
		tmp.append(this.giroEnDescubiertoHabilidado());
		return tmp.toString();}

	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(CuentaCorriente.class)) {
			CuentaCorriente other = (CuentaCorriente) obj;
			return (this.id == other.id) &&
					(this.cliente.equals(other.cliente)) &&
					(this.saldo == other.saldo)&&
					(this.giroEnDescubierto == other.giroEnDescubierto);
		} else {
			return false;
		}
	}
}
