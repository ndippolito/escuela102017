package pablo;

public abstract class Cuenta implements dipi.ICuenta {

	protected long id;
	protected Cliente cliente;
	protected long saldo;

	public Cuenta(long id, Cliente cliente) {
		this.id = id;
		this.cliente = cliente;
		this.saldo = 0;
	}

	public long getId() {
		return this.id;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public long getSaldo() {
		return this.saldo;
	}

	public boolean depositar(long montoADepositar) throws dipi.MontoNegativoCuentaException {
		this.saldo = this.saldo + montoADepositar;
		return true;
	}

	public boolean extraer(long montoAExtraer) throws dipi.MontoNegativoCuentaException {
		boolean seExtrajo;
		if (montoAExtraer <= this.saldo) {
			this.saldo = this.saldo - montoAExtraer;
			seExtrajo = true;
		} else {
			seExtrajo = false;
		}
		return seExtrajo;
	}

	@Override
	public String toString() {
		StringBuffer tmp = new StringBuffer();
		tmp.append("[");
		tmp.append(this.getId());
		tmp.append(", ");
		tmp.append(this.cliente.toString());
		tmp.append(", ");
		tmp.append(this.getSaldo());
		tmp.append("]");
		return tmp.toString();
		}
	
	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(Cuenta.class)) {
			Cuenta other = (Cuenta) obj;
			return (this.id == other.id) &&
					(this.cliente.equals(other.cliente)) &&
					(this.saldo == other.saldo);
		} else {
			return false;
		}
	}
}