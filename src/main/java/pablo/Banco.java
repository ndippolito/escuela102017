package pablo;

import java.util.Set;
import java.util.HashSet;
import dipi.ICliente;
import dipi.ICuenta;

public class Banco implements dipi.IBanco{

	private String nombre;
	private Set<ICuenta> cuentas;
	private Set<ICliente> clientes;
	
	public Banco(String nombre) {
		this.nombre = nombre;
		this.cuentas = new HashSet<ICuenta>();
		this.clientes = new HashSet<ICliente>();
	}
	
	public void agregarCliente(ICliente cliente) {
		clientes.add(cliente);
		cuentas.addAll(cliente.getCuentas());
		
	}

	public Set<ICuenta> getCuentas() {
		return this.cuentas;
	}

	public Set<ICliente> getClientes() {
		return this.clientes;
	}

	public String getNombre() {
		return this.nombre;
	}

	@Override
	public String toString() {
		StringBuffer tmp = new StringBuffer();
		tmp.append("[");
		tmp.append(this.getNombre());
		tmp.append(", ");
		tmp.append(this.getClientes().toString());
		tmp.append("]");
		return tmp.toString();}
	
	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(Banco.class)) {
			Banco other = (Banco) obj;
			return 	(this.nombre.equals(other.nombre)) &&
					(this.clientes.equals(other.clientes)) &&
					(this.cuentas.equals(other.cuentas));
			
		} else {
			return false;
		}
	}

}