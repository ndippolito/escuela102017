package pablo;

import java.lang.Character;

public class Persona {
	private long DNI;
	private String nombre;
	private String domicilio;
	
	public Persona(long DNI) {
		this.DNI = DNI;
		this.nombre = "";
		this.domicilio = "";
	}
	
	public Persona(long DNI, String nombre, String domicilio) {
		this.DNI = DNI;
		
		boolean res = true;
		for (int i = 0; i < nombre.length(); i++) {
			Character letra = nombre.charAt(i);
			if (!Character.isLetter(letra) && !Character.isWhitespace(letra)) {
				res = false;
			}
		}
		
		if (res) {
			this.nombre = nombre;
		} else {
			this.nombre = "";
		}
	
		this.domicilio = domicilio;
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		boolean res = true;
		for (int i = 0; i < nombre.length(); i++) {
			Character letra = nombre.charAt(i);
			if (!Character.isLetter(letra) && !Character.isWhitespace(letra)) {
				res = false;
			}
		}
		if (res) {
			this.nombre = nombre;
		} else {
			this.nombre = "";
		}
	}

	public long getDNI() {
		return this.DNI;
	}

	public String getDomicilio() {
		return this.domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	@Override
	public String toString() {
		StringBuffer tmp = new StringBuffer();
		tmp.append("[");
		tmp.append(this.nombre);
		tmp.append(", ");
		tmp.append(this.DNI);
		tmp.append(", ");
		tmp.append(this.domicilio);
		tmp.append("]");
		return tmp.toString();
	}

	
	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(Persona.class)) {
			Persona other = (Persona) obj;
			return 	(this.getDNI() == other.getDNI()) && 
					(this.getNombre().equals(other.getNombre())) &&
					(this.getDomicilio().equals(other.getDomicilio()));
			} else {
			return false;
		}
	}
		
		
	

}