package pablo.Ajedrez;

import static org.junit.Assert.*;

import org.junit.Test;

public class AjedrezTest {

	@Test
	public void AjedrezTestConstructor() {
		Ajedrez unJuego = new Ajedrez();
		assertEquals(unJuego.getPiezas().size(), 16);
	}
	
	@Test
	public void AjedrezTestMoverPieza() {
		Ajedrez unJuego = new Ajedrez();
		unJuego.moverPieza(1, 0, 2, 0);
		unJuego.moverPieza(6, 0, 5, 0);
		unJuego.moverPieza(2, 0, 3, 0);
		unJuego.moverPieza(5, 0, 4, 0);
		unJuego.moverPieza(3, 0, 4, 0);
		
	}

}
