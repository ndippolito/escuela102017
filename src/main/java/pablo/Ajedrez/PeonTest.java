package pablo.Ajedrez;

import static org.junit.Assert.*;

import org.junit.Test;

public class PeonTest {

	@Test
	public void PeonTestConstructor() {
		Peon unPeon = new Peon(1,2,true);
		assertEquals(unPeon.getPosicion().fila, 1);
		assertEquals(unPeon.getPosicion().columna, 2);
	}
	
	@Test
	public void PeonTestMover() {
		Peon unPeon = new Peon(1,2,true);
		assertTrue(unPeon.esMovimientoValido(2, 2));
		assertFalse(unPeon.esMovimientoValido(1, 3));
		unPeon.mover(2, 2);
		assertEquals(unPeon.getPosicion().fila, 2);
		assertEquals(unPeon.getPosicion().columna, 2);
		
	}
	

}
