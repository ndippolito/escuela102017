package pablo.Ajedrez;

public class Peon implements IPieza{
	private boolean equipo;
	private Posicion pos;
	
	
	public Peon(int f, int c, boolean team) {
		this.equipo = team;
		this.pos = new Posicion(f,c);
	}
	
	public Posicion getPosicion() {
		return this.pos;
	}
	
	public boolean esMovimientoValido(int f, int c) {
		return (((this.equipo && this.pos.fila +1 == f) ||
				!this.equipo && this.pos.fila -1 == f)) &&
				this.pos.columna == c;
	}

	public void mover(int f, int c) {
		this.pos.fila = f;
		this.pos.columna = c;
	}

	public boolean getEquipo() {
		return this.equipo;
	}

	
	
}