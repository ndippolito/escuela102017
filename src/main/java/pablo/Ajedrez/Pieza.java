package pablo.Ajedrez;

public abstract class Pieza {
	protected boolean equipo;
	protected Posicion pos;
	
	public Pieza(int f, int c, boolean team) {
		Posicion pos = new Posicion(f,c);	
		this.equipo = team;
		this.pos = pos;
	}

	public Posicion getPosicion() {
		return pos;
		
	}

	public void mover(int f, int c) {
			this.pos.fila = f;
			this.pos.columna = c;
	}

	public boolean esMovimientoValido(int nf, int nc) {
		return true;
	}
	
	
}
