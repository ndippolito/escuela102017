package pablo.Ajedrez;

public interface IPieza {
	public Posicion getPosicion();
	public boolean getEquipo();
	public boolean esMovimientoValido(int f, int c);
	public void mover(int f, int c);

}
