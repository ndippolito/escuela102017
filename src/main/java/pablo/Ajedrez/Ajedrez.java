package pablo.Ajedrez;

import java.util.LinkedList;
import java.util.List;

public class Ajedrez {

	private List<IPieza> piezasVivas;
	// true es blanco y false es negro;
	private boolean turno;

	public Ajedrez() {

		this.piezasVivas = new LinkedList<IPieza>();

		// Inicializa peones blancos
		for (int i = 0; i < 8; i++) {
			this.piezasVivas.add(new Peon(1, i, true));
		}

		// Inicializa peones negros
		for (int i = 0; i < 8; i++) {
			this.piezasVivas.add(new Peon(6, i, false));
		}

		// Inicializa turno en blanco
		this.turno = true;

	}

	public List<IPieza> getPiezas() {
		return this.piezasVivas;
	}

	// Asume como precondicion que (f,c) y (nf,nc) forman casillas valida
	// y en (f,c) hay una pieza del equipo que juega.
	public void moverPieza(int f, int c, int nf, int nc) {
		// Busca la pieza a mover
		int i;
		for (i = 0; i < piezasVivas.size(); i++) {
			if (piezasVivas.get(i).getPosicion().fila == f && piezasVivas.get(i).getPosicion().columna == c) {
				break;
			}
		}

		// Chequea si el movimiento es valido para la pieza
		boolean esValido = piezasVivas.get(i).esMovimientoValido(nf, nc);

		// Chequea si el destino esta ocupado y por quien
		boolean estaOcupado = false;
		boolean sePuedeMover = true;
		int j;
		if (esValido) {
			for (j = 0; j < piezasVivas.size(); j++) {
				if (piezasVivas.get(j).getPosicion().fila == nf && piezasVivas.get(j).getPosicion().columna == nc) {
					estaOcupado = true;
					if (piezasVivas.get(j).getEquipo() == turno) {
						sePuedeMover = false;
					}
					break;
				}
				// Si se cumplen ambas condiciones, avanza y, si es necesario, come
				if (esValido && sePuedeMover) {
					piezasVivas.get(i).mover(nf, nc);
					if (estaOcupado) {
						piezasVivas.remove(j); //FIXME avanza bien pero no come
					}
				}
			}
		}
	}
}