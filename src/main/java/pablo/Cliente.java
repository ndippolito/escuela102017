package pablo;

import java.util.Set;
import java.util.HashSet;
import dipi.ICuenta;

public class Cliente extends Persona implements dipi.ICliente{
	private Set<ICuenta> cuentas;
	
	public Cliente(long DNI, String nombre, String domicilio) {
		super(DNI, nombre, domicilio);
		this.cuentas = new HashSet<ICuenta>();
	}

	public void agregarCuenta(ICuenta cuenta) {
		if(cuenta.getCliente().equals(this)) {
			this.cuentas.add(cuenta);
		}	
	}

	public Set<ICuenta> getCuentas() {
		return this.cuentas;
	}

	public String getNombre() {
		return super.getNombre();
	}

	@Override
	public String toString() {
		StringBuffer tmp = new StringBuffer();
		tmp.append("[");
		tmp.append(super.getNombre());
		tmp.append(", ");
		tmp.append(super.getDNI());
		tmp.append(", ");
		tmp.append(super.getDomicilio());
		tmp.append(", ");
		tmp.append(this.getCuentas());
		tmp.append("]");
		return tmp.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(Cliente.class)) {
			Cliente other = (Cliente) obj;
			return 	(this.getDNI() == other.getDNI()) && 
					(this.getNombre().equals(other.getNombre())) && 
					(this.getDomicilio().equals(other.getDomicilio())) &&
					(this.cuentas.equals(other.cuentas));
			} else {
			return false;
		}
	}
	
	

}