package lucas;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import dipi.ICliente;
import dipi.ICuenta;

public class Cliente implements ICliente {

	private String nombre;
	private Set<ICuenta> cuentas;
	
	public Cliente(String nombre) {
        if(nombre != null && Pattern.matches("[a-zA-Z]+", nombre)) {      
        	this.nombre = nombre;
        } else {
                this.nombre = "";
        }
		cuentas = new HashSet<ICuenta>();
	}
	
	public void agregarCuenta(ICuenta cuenta) {
		if(!(cuentas.contains(cuenta)) && this.equals(cuenta.getCliente())) {
			this.cuentas.add(cuenta);
		}
	}

	public Set<ICuenta> getCuentas() {
		return cuentas;
	}

	public String getNombre() {
		return nombre;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("Nombre= " + nombre);
		buffer.append(" Cuentas: ");
		for(ICuenta cuenta : getCuentas()) {
			buffer.append("id= "+cuenta.getId() + " saldo= " +cuenta.getSaldo());
		}
		return buffer.toString();
	}
	
	@Override
	public boolean equals(Object otro) {
		if(this == otro) {
			return true;
		}
		if(otro == null) {
			return false;
		}
		if(getClass() != otro.getClass()){
			return false;
		}
		Cliente otrocliente = (Cliente) otro;
		return (this.cuentas.equals(otrocliente.cuentas) && nombre.equals(otrocliente.nombre));
		}

}
