package lucas;

import java.util.HashSet;
import java.util.Set;

import dipi.IBanco;
import dipi.ICliente;
import dipi.ICuenta;

public class Banco implements IBanco {

	private String nombre;
	private Set<ICliente> clientes;
	private Set<ICuenta> cuentas;

	public Banco(String nombre) {
		if(nombre != null) {
			this.nombre = nombre;
		} else {
			this.nombre = "Bancoo1";
		}
		clientes = new HashSet<ICliente>();
		cuentas = new HashSet<ICuenta>();
	}
	
	public void agregarCliente(ICliente cliente) {
		clientes.add(cliente);
		for(ICuenta cuenta : cliente.getCuentas()) {
			cuentas.add(cuenta);
		}
	}

	public Set<ICuenta> getCuentas() {
		return cuentas;
	}

	public Set<ICliente> getClientes() {
		return clientes;
	}

	public String getNombre() {
		return nombre;
	}
	
	@Override
	public String toString() {
		StringBuffer clientes = new StringBuffer();
		StringBuffer cuentas = new StringBuffer();
		
		for(ICliente cliente : getClientes()) {
			clientes.append(cliente.getNombre()+", ");
		}
		if(clientes.length()>0) {
			clientes.substring(0, clientes.length()-2);
		}
		if(cuentas.length()>0) {
			cuentas.substring(0,cuentas.length()-2);
		}
		for(ICuenta cuenta : getCuentas()) {
			cuentas.append(cuenta.getId()+", ");
		}
		return "Banco "+ nombre+ " ,clientes: "+ clientes + ", cuentas: " + cuentas ; 
	}
	
	@Override
	public boolean equals(Object otro) {
		if(this == otro) {
			return true;
		}
		if(otro == null) {
			return false;
		}
		if(getClass() != otro.getClass()) {
			return false;
		}
		Banco otrobanco = (Banco)otro;
		return (this.nombre.equals(otrobanco.nombre) &&
				this.clientes.equals(otrobanco.clientes) &&
				this.cuentas.equals(otrobanco.cuentas));
	}

}
