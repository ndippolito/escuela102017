package lucas;

import dipi.ICliente;
import dipi.ICuentaCorriente;
import dipi.MontoNegativoCuentaException;

public class CuentaCorriente extends Cuenta implements ICuentaCorriente {
	private long giroendescubierto;

	public CuentaCorriente(long id, Cliente cliente, long saldo, long limitedescubierto)
			throws idNegativoExceptions, idRepetidoExceptions {
		super(id, cliente, saldo);
		if(limitedescubierto>1000) {
			this.giroendescubierto = limitedescubierto;
		} else {
			this.giroendescubierto = 1000;
		}
	}
	
	public long getId() {
		return super.getId();
	}

	public ICliente getCliente() {
		return super.getCliente();
	}

	public long getSaldo() {
		return super.getSaldo();
	}

	@Override
	public boolean depositar(long montoADepositar) throws MontoNegativoCuentaException {
		return super.depositar(montoADepositar);
	}
	
	@Override
	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {
		if(montoAExtraer > giroendescubierto + super.getSaldo() || montoAExtraer < 0) {
			throw new MontoNegativoCuentaException();			
		}
		return super.descontarSaldo(montoAExtraer);
	}

	public long giroEnDescubiertoHabilidado() {
		return giroendescubierto;
	}
	
	@Override
	public String toString() {
		return "Titular= "+  getCliente().getNombre() + 
				", idcuenta= " + getId()+ ", saldo= " +getSaldo() +
				", giro en descubierto= " + giroEnDescubiertoHabilidado();
	}
	
	@Override
	public boolean equals(Object otro) {
		if(this == otro) {
			return true;
		}
		if(otro == null) {
			return false;
		}
		if(getClass() != otro.getClass()) {
			return false;
		}
		CuentaCorriente otra = (CuentaCorriente) otro;
		return (super.equals(otro) &&
				giroendescubierto == otra.giroendescubierto);
	}

}
