package lucas;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Persona {
	private String nombre;
	private Long dni;
	private String domicilio;
	
	public Persona(String nombre, Long dni, String domicilio) {
		this.nombre = nombre;
		this.dni = dni;
		this.domicilio = domicilio; 
	}
	
	public Persona(Long dni) {
		this.dni = dni;
		this.nombre = "";
		this.domicilio = "";
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		Pattern p = Pattern.compile("[a-z]");
		Matcher m = p.matcher(nombre);
		if(m.find()) {
			this.nombre = nombre;
		} else {
			this.nombre = "";
		}
	}
	public long getDni() {
		return dni;
	}
	public void setDni(Long dni) {
		this.dni = dni;
	}
	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	
	@Override
	public String toString() {
		return "nombre= " + this.nombre + ", dni= " + this.dni + ", domicilio= " + this.domicilio;
	}
	@Override
	public boolean equals(Object otro) {
		boolean iguales = false;
		if(otro instanceof Persona) {
			Persona otropersona = (Persona) otro;
			iguales = (this.nombre.equals(otropersona.nombre) && 
						this.dni.equals(otropersona.dni) &&	
						this.domicilio.equals(otropersona.domicilio));
		}
		return iguales;
	}
	
	
}
