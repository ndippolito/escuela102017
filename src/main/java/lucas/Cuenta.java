package lucas;

import java.util.Iterator;

import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public class Cuenta implements ICuenta {

	private long id;
	private Cliente titular;
	private long saldo;
	
	public Cuenta(long id, Cliente titular, long saldo) 
			throws idNegativoExceptions, idRepetidoExceptions {
		if(idExistente(titular,id)) {
			throw new idRepetidoExceptions();
		}
		if(id<0) {
			throw new idNegativoExceptions();
		}
		this.id = id;
		this.titular = titular;
		if(saldo > 0) {
			this.saldo = saldo;
		} else {
			this.saldo = 0;
		}
	}

	private boolean idExistente(Cliente titular, long id2) {
		Iterator<ICuenta> it = titular.getCuentas().iterator();
		boolean existeId = false;
		while(it.hasNext() && !existeId) {
			existeId = it.next().getId() == id2;
		}
		return existeId;
	}

	public long getId() {
		return id;
	}

	public ICliente getCliente() {
		return titular;
	}

	public long getSaldo() {
		return saldo;
	}

	public boolean depositar(long montoADepositar) throws MontoNegativoCuentaException {
		boolean efectuado = false;
		if(montoADepositar < 0) {
			throw new MontoNegativoCuentaException();
		} else {
			this.saldo += montoADepositar;
			efectuado = true;
		}
		return efectuado;
	}

	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {
		boolean efectuado = false;
		long futurosaldo = this.saldo - montoAExtraer;
		if(montoAExtraer >= 0 && futurosaldo > -1) {
			this.saldo = futurosaldo;
			efectuado = true;
		} else {
			throw new MontoNegativoCuentaException();
		}
		return efectuado;
	}
	
	@Override
	public String toString() {
		return "Titular= "+  getCliente().getNombre() + ", idcuenta= " + getId()+ ", saldo= " +getSaldo();
	}
	
	@Override
	public boolean equals(Object otro) {
		if(this == otro) {
			return true;
		}
		if(otro == null) {
			return false;
		}
		if(getClass() != otro.getClass()) {
			return false;
		}
		Cuenta otracuenta = (Cuenta) otro;
		return (id == otracuenta.id && 
		  	    saldo == otracuenta.saldo &&
				titular.equals(otracuenta.titular));
	}

	public boolean descontarSaldo(long montoAExtraer) {
		this.saldo -= montoAExtraer;
		return true;
	}

}
