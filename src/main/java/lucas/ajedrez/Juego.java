package lucas.ajedrez;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;


public class Juego {

	private String jugador1;
	private String jugador2;
	private Hashtable<String,Set<Pieza>> piezajugador;
	
	public Juego(String jugador1, String jugador2, List<Pieza> piezas) {
		//las piezas que ingresan tiene que ser las correctas para jugar.
		//piezas tiene que estar ordenada por ubicacion en el tablero, por filas.
		//ej : torre,peon,caballo,peon...
		this.jugador1 = jugador1;
		this.jugador2 = jugador2;
		piezajugador.put(jugador1, acomodarPiezas(jugador1,piezas));
		piezajugador.put(jugador2, acomodarPiezas(jugador2,piezas));
		
	}
	
		
	
	private Set<Pieza> acomodarPiezas(String jugador12, List<Pieza> piezas) {
		Set<Pieza> piezasxjug = new HashSet();
		if(this.jugador1.equals(jugador12)) {
			Pair<Integer,Integer> posicion  = new Pair<Integer,Integer>();
			for(Pieza pieza : piezas) {
				//tengo que usar establecer pos y asignarle su respectivo lugar.
			}
		} else {
			//lo mismo que con jugador1 pero invirtiendo las posiciones
		}
		return piezasxjug;
	}

	public void movimiento(String jugador, Pair<Integer,Integer> movimiento) {
		
	}

	public Set<Pieza> piezasJugador(String jugador){
		return piezajugador.get(jugador);
	}
	
	public Pair<Integer,Integer> posicion(String jugador, Pieza pieza){
		if(piezajugador.get(jugador).contains(pieza)){
			Pair<Integer,Integer> pos = new Pair<Integer,Integer>();
			for(Pieza busco : piezajugador.get(jugador)) {
				if(busco.equals(pieza)) {
					pos = busco.getPosicion();
				}
			}
			return pos;
		} else {
			Pair<Integer,Integer> posInvalida = new Pair<Integer,Integer>(-100,-100);
			return posInvalida;
		}
	}
	
}
