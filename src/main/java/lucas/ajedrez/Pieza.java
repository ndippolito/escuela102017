package lucas.ajedrez;


public class Pieza{
	
	private String nombre;
	private Pair<Integer,Integer> posicion;
	
	public Pieza(String nombre) {
		this.nombre = nombre;
		posicion = new Pair<Integer,Integer>();
	}
	
	public void establecerPos(Pair<Integer,Integer> posicion) {
		posicion.setPrim(posicion.getPrim());
		posicion.setSeg(posicion.getSeg());
	}
	
	public String getNombre(){
		return this.nombre;
	}
	
	public void mover(Pair<Integer,Integer> movimiento) throws movimientoIncorrectoExceptions{
		if(movimientoPosible(movimiento)) {
			this.posicion.setPrim(movimiento.getPrim());
			this.posicion.setSeg(movimiento.getSeg());
		} else {
			throw new movimientoIncorrectoExceptions();
		}
	}
	
	private boolean movimientoPosible(Pair<Integer, Integer> movimiento) {
		if(nombre.equals("Peon")) {
			return (movimiento.getPrim() != movimiento.getSeg() &&
					movimiento.getPrim() > 0 && movimiento.getSeg() == 0);
		}
		if(nombre.equals("Rey")) {
			return (movimiento.getPrim() < 2 && movimiento.getPrim() > -2 && 
					movimiento.getSeg() < 2 && movimiento.getSeg() > -2);
		}
		if(nombre.equals("Alfil")) {
			return (movimiento.getPrim() == movimiento.getSeg());
		}
		if(nombre.equals("Torre")){
			return (movimiento.getPrim() != 0 && movimiento.getPrim() == 0 || 
					movimiento.getSeg() == 0 && movimiento.getSeg() != 0);
		}
		return false;
	}

	public Pair<Integer,Integer> getPosicion(){
		return posicion;
	}
	
}
