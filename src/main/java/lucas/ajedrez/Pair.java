package lucas.ajedrez;

public class Pair<T1, T2> {
	private int prim;
	private int seg;
	
	public Pair() {
		this.prim = 0;
		this.seg = 0;
	}
	
	public Pair(int prim, int seg) {
		this.prim = prim;
		this.seg = seg;
	}
	public int getPrim() {
		return prim;
	}
	public int getSeg() {
		return seg;
	}
	public void setPrim(int prim) {
		this.prim += prim;
	}
	public void setSeg(int seg) {
		this.seg += seg;
	}
}
