package Ariel;

public class Persona {
	private long dni;
	private String nombre;
	private String domicilio;
	
	public Persona(long dni, String nombre, String domicilio) {
		this.dni = dni;
		this.nombre = nombre;
		this.domicilio = domicilio;
	}
	
	public Persona(Long dni) {
		this.dni = dni;
	}
	
	
	public long getDni() {
		return this.dni;
	}
	//public void setDni(Long dni) {
	//	this.dni = dni;
	//}
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDomicilio() {
		return this.domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	@Override
	public String toString() {
		return "Persona [dni=" + this.getDni() + ", nombre=" + this.getNombre() + ", domicilio=" + this.getDomicilio() + "]";
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (dni != other.dni)
			return false;
		if (domicilio == null) {
			if (other.domicilio != null)
				return false;
		} else if (!domicilio.equals(other.domicilio))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	
	
	
	

}
