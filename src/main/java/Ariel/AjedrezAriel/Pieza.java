package Ariel.AjedrezAriel;

public abstract class Pieza {
	protected int y;
	protected int x;
	protected boolean estado;
	
	public Pieza( int x,int y) {
		this.y = y;
		this.x = x;
		this.estado = true;
	}
	protected void setPosicionY(int y) {
		this.y = y;
	}
	protected void setPosicionX(int x) {
		this.x = x;
	}
	protected void setEstado() {
		this.estado = !this.estado;
	}
	
	public boolean getEstado() {
		return this.estado;
	}
	public int getPosicionX() {
		return this.x;
	}
	public int getPosicionY() {
		return this.y;
	}
	public int[] getPosicion() {
		int[] posicion = new int[2];
		posicion[0] = this.getPosicionY();
		posicion[1] = this.getPosicionX();
		return posicion;
		
	}
	public abstract boolean movimientoPermitido(int x , int y);
	public abstract boolean moverPosicion(int x,int y, Ajedrez tablero);
	
	
}
