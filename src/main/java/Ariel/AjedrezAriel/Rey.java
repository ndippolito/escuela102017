package Ariel.AjedrezAriel;

public class Rey extends Pieza{

	public Rey(int y, int x) {
		super(y, x);
	}
	
	@Override
	public boolean moverPosicion(int x , int y, Ajedrez tablero) {
		if (this.movimientoPermitido(x, y)) {
			this.setPosicionX(x);
			this.setPosicionY(y);
			return true;
		}else {
			return false;
		}
	}
	 
	protected boolean movimiento(int actualG, int siguienteG) {
		if(siguienteG >= 0 && siguienteG < 8){
			if((siguienteG > actualG) && ( (siguienteG - actualG) == 1 )) {
				return true;
			}else if((siguienteG < actualG) && (actualG != 0 ) && (( actualG - siguienteG  ) == 1 )) {
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}	
	}

	@Override
	public boolean movimientoPermitido(int x , int y) {
		if((this.movimiento(this.x, x)) && (this.movimiento(this.y, y))){
			return true;
		}else {
			return false;
		}
	}
	
}
