package Ariel.AjedrezAriel;

public class Peon extends Rey{

	public Peon(int y, int x) {
		super(y, x);
	}
	protected boolean movimiento(int actualG, int siguienteG) {
		if(siguienteG >= 0 && siguienteG < 8){
			if((siguienteG > actualG) && ( (siguienteG - actualG) == 1 )) {
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}	
	}
	
}
