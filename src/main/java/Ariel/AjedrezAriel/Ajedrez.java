package Ariel.AjedrezAriel;

import java.util.HashSet;
import java.util.Set;

public class Ajedrez {
	public Pieza[][] tablero;
	private Jugador jugador1;
	private Jugador jugador2;
	
	public Ajedrez() {
		this.tablero = new Pieza[8][8];
	}
	
	public boolean posicionPermitida(int x,int y) {
		if(tablero[x][y]== null) {
			return true;
		}else {
			return false;
		}
	}
	public boolean ocuparPosicion(Pieza obj,int x,int y) {
		if(this.posicionPermitida(x, y)) {
			this.tablero[x][y]= obj;
			return true;
		}else {
			return false;
		}
	}
	public void agregarJugador1(Jugador jugador1) {
		this.jugador1 = jugador1;
		jugador1.setTablero(this);
		
	}
	public void agregarJugador2(Jugador jugador2) {
		this.jugador2 = jugador2;
		jugador2.setTablero(this);
	}
	public void posicionarPeones(int fila,int columna,Jugador jugador){
		Set<Pieza> piezas = new HashSet<Pieza>();
		for (int i=0 ; i < fila; i++) {
			Peon peon = new Peon(fila, columna);
			tablero[columna][i] = peon;
			piezas.add(peon);
		}
		jugador.setPiezas(piezas);
	}
	public void posicionarPrincipales(int columna,Jugador jugador) {
		Set<Pieza> piezas = new HashSet<Pieza>();
		Torre torre = new Torre(0,columna);
		piezas.add(torre);
		torre = new Torre(7,columna);
		piezas.add(torre);
		
		Alfil alfil = new Alfil(2,columna);
		piezas.add(alfil);
		alfil = new Alfil(5,columna);
		piezas.add(alfil);
		
		Caballo caballo = new Caballo(1,columna);
		piezas.add(caballo);
		caballo = new Caballo(6,columna);
		piezas.add(caballo);
		
		Rey rey = new Rey(4,columna);
		piezas.add(rey);

		Reina reina = new Reina(3,columna);
		piezas.add(reina);
		
	}
	public void iniciarJuego() {
		this.posicionarPrincipales(0, this.jugador1);
		this.posicionarPeones(8, 1, this.jugador1);
		this.posicionarPrincipales(7, this.jugador2);
		this.posicionarPeones(8, 6, this.jugador2);
		
	}
}
