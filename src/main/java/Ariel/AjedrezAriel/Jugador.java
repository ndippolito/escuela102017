package Ariel.AjedrezAriel;

import java.util.Set;

public class Jugador {
	private String nombre;
	private Set<Pieza> piezas;
	private Ajedrez tablero;
	public Jugador(String nombre) {
		this.nombre = nombre;
	}
	public void setTablero(Ajedrez tablero){
		this.tablero = tablero;
	}
	public void setPiezas(Set<Pieza> piezas){
		this.piezas.addAll(piezas);
	}
	public Set<Pieza> getPiezas() {
		return this.piezas;
		
	}
	
	
	
}
