package Ariel;
import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public abstract  class Cuenta implements ICuenta {
	private long id;
	private long saldo;
	private ICliente cliente;
	
	public Cuenta(long id, long saldo, ICliente cliente) throws MontoNegativoCuentaException {
		this.setId(id);
		this.setSaldo(saldo);
		this.setCliente(cliente);
	}
	private void setId(long id) throws MontoNegativoCuentaException {//
		if(id > 0) {
			this.id = id;
		}else {
			throw new MontoNegativoCuentaException();
		}
	}
	private void setCliente(ICliente cliente) {//
		if(cliente != null) {
			this.cliente = cliente;
		}else {
        	throw new NullPointerException();
        } 
	}
	
	
	public long getId() {
		return this.id;
	}

	public ICliente getCliente() {
		return this.cliente;
	}

	public long getSaldo() {
		return this.saldo;
	}
	
	public boolean depositar(long montoADepositar) throws MontoNegativoCuentaException {
		if(montoADepositar >= 0){
			this.setSaldo( this.getSaldo() + montoADepositar );
			return true;
		}else {
			throw new MontoNegativoCuentaException();
		}
	}
	
	protected void setSaldo(long saldo){//
		this.saldo=saldo;
	}
	public boolean equals(Cuenta obj) {
		if (this == obj
			&& this.getId() == obj.getId()
			&& this.getSaldo() == obj.getSaldo() 
			&& this.getCliente() == obj.getCliente() ){
			return true;
		}else {
			return false;
		}
	}
	
	public abstract boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException;
	
	@Override
	public String toString() {
		return "Id=" + this.getId() + ", Saldo=" + this.getSaldo() + ", Cliente=" + this.getCliente().getNombre();
	}
	
	@Override
	public abstract boolean equals(Object obj);
	
	
	

}
	
