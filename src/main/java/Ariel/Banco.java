package Ariel;

import java.util.HashSet;
import java.util.Set;

import dipi.IBanco;
import dipi.ICliente;
import dipi.ICuenta;

public class Banco implements IBanco{
	private String nombre;
	private Set<ICliente> clientes;
	private Set<ICuenta> cuentas;
	
	public Banco(String nombre) throws NombreNoValidoException {
		this.setNombre(nombre);
		this.clientes = new HashSet<ICliente>();
		this.cuentas = new HashSet<ICuenta>();
	}

	private void setNombre(String nombre)throws NombreNoValidoException{
		/*if( nombre.matches("([a-z]|[A-Z]|\\s)+") ) {
			this.nombre = nombre;
		}//NullPointerException*/
		if(nombre != null) {      
            if ( nombre.matches("([a-z]|[A-Z]|\\s)+") ) {
                this.nombre = nombre;
            } else {
                throw new NombreNoValidoException();
            }
        }
        else {
        	throw new NullPointerException();
        }
	}
	public void agregarCliente(ICliente cliente) {
		if(cliente != null) {
			this.clientes.add(cliente);
			this.cuentas.addAll(cliente.getCuentas());
		}else {
        	throw new NullPointerException();
        }
	}
	public Set<ICuenta> getCuentas() {
		return this.cuentas;
	}

	public Set<ICliente> getClientes() {
		return this.clientes;
	}

	public String getNombre() {
		return this.nombre;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Banco other = (Banco) obj;
		if (clientes == null) {
			if (other.clientes != null)
				return false;
		} else if (!clientes.equals(other.clientes))
			return false;
		if (cuentas == null) {
			if (other.cuentas != null)
				return false;
		} else if (!cuentas.equals(other.cuentas))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}


	/*public boolean equals(Banco obj) {
		if (this == obj 
			&& this.getClientes().equals(obj.getClientes())
			&& this.getCuentas().equals(obj.getCuentas()) 
			&& this.getNombre().equals(obj.getNombre())) {
			return true;
		}else {
			return false;
		}
	}*/
	
	
	
	
}
