package Ariel;

import dipi.ICliente;
import dipi.ICuentaCorriente;
import dipi.MontoNegativoCuentaException;

public class CuentaCorriente extends Cuenta implements ICuentaCorriente{

	private long descubiertoHabilitado;

	public CuentaCorriente(long id, long saldo, ICliente cliente) throws MontoNegativoCuentaException {
		super(id, saldo, cliente);
		this.setDescubiertoHabilitado(saldo / 2);
	}
	public CuentaCorriente(long id, long saldo, ICliente cliente,long descubiertoHabilitado) throws MontoNegativoCuentaException {
		super(id, saldo, cliente);
		this.descubiertoHabilitado = descubiertoHabilitado;
	}

	private void setDescubiertoHabilitado(long saldo) throws MontoNegativoCuentaException {
		if(saldo > 0){
			this.descubiertoHabilitado = saldo;
		}else  {
			throw new MontoNegativoCuentaException();
		}
		
	}
	@Override
	protected void setSaldo(long saldo) {
		if(saldo > 0) {
			super.setSaldo(saldo);
		}
	}
	@Override
	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {
		
		if(this.getSaldo() >= montoAExtraer && (montoAExtraer > 0 ) ) {
			this.setSaldo( this.getSaldo() - montoAExtraer);
			return true;
		}else {
			
			if( (this.getSaldo() > (- this.giroEnDescubiertoHabilidado() ) ) 
				&& (montoAExtraer > 0 )
				&& ( this.getSaldo() + this.giroEnDescubiertoHabilidado() ) >= montoAExtraer) {
					long saldoConGiroEnDescubierto = (montoAExtraer - this.getSaldo() );
					super.setSaldo( -saldoConGiroEnDescubierto );
					return true;
			}else {
				return false;
			}
		}
	}
	public long giroEnDescubiertoHabilidado() {
		return this.descubiertoHabilitado;
	}

	/*public boolean equals(CuentaCorriente obj) {
		if (this == obj
			&& this.getId() == obj.getId()
			&& this.getSaldo() == obj.getSaldo() 
			&& this.getCliente().equals( obj.getCliente()) 
			&& this.giroEnDescubiertoHabilidado() == obj.giroEnDescubiertoHabilidado() ){
			return true;
		}else {
			return false;
		}
	}*/
	@Override
	public String toString() {
		return "CuentaCorriente ["+ super.toString()
				+ " descubiertoHabilitado = " + this.giroEnDescubiertoHabilidado() + " ]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CuentaCorriente other = (CuentaCorriente) obj;
		if (this.getCliente() == null) {
			if (other.getCliente() != null)
				return false;
		} else if (!this.getCliente().equals(other.getCliente()))
			return false;
		if (this.getId() != other.getId())
			return false;
		if (this.getSaldo() != other.getSaldo())
			return false;
		if (this.giroEnDescubiertoHabilidado() != other.giroEnDescubiertoHabilidado())
			return false;
		return true;
	}
	
	
	
}
