package Ariel;

import dipi.ICliente;
import dipi.MontoNegativoCuentaException;

public class CajaDeAhorro extends Cuenta{

	public CajaDeAhorro(long id, long saldo, ICliente cliente) throws MontoNegativoCuentaException {
		super(id, saldo, cliente);
	}
	
	@Override
	protected void setSaldo(long saldo) {
		if(saldo > 0) {
			super.setSaldo(saldo);
		} else {
			try {
				throw new MontoNegativoCuentaException();
			} catch (MontoNegativoCuentaException e) {
				//System.out.println("Monto Negativo");;
			}
		}
	}
	@Override
	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException{
		long saldoRestante = this.getSaldo() - montoAExtraer;
		
		if (  montoAExtraer >= 0 && saldoRestante >= 0)  {
			this.setSaldo(saldoRestante);
			return true;
		}else {
			return false;
		}
	}
	
	public boolean equals(CajaDeAhorro obj) {
		
		if(this == obj 
			&& this.getId() == obj.getId() 
			&& (this.getSaldo() == obj.getSaldo()) 
			&& (this.getCliente().equals(obj.getCliente())) ) {
			return true;
		}else{
			return false;
		}
		
	}

	@Override
	public String toString() {
		return "CajaDeAhorro [ " + super.toString() + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CajaDeAhorro other = (CajaDeAhorro) obj;
		if (this.getCliente() == null) {
			if (other.getCliente() != null)
				return false;
		} else if (!this.getCliente().equals(other.getCliente()))
			return false;
		if (this.getId() != other.getId())
			return false;
		if (this.getSaldo() != other.getSaldo())
			return false;
		return true;
	}
	

}
