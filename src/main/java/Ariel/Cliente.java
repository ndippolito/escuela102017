package Ariel;

import java.util.HashSet;
import java.util.Set;
import dipi.ICliente;
import dipi.ICuenta;

public class Cliente implements ICliente {
	
	private String nombre;
	private Set<ICuenta> cuenta;
	
	public Cliente(String nombre) throws NombreNoValidoException {
		this.setNombre(nombre);
		this.cuenta = new HashSet<ICuenta>();
	}

	private void setNombre(String nombre) throws NombreNoValidoException {
		/*if( nombre.matches("([a-z]|[A-Z]|\\s)+") ) {
			this.nombre = nombre;
		}*/
		if(nombre != null) {      
            if ( nombre.matches("([a-z]|[A-Z]|\\s)+") ) {
                this.nombre = nombre;
            } else {
                throw new NombreNoValidoException();
            }
        }
        else {
        	throw new NullPointerException();
        } 
	}
	
	public void agregarCuenta(ICuenta cuenta) {
		if(cuenta != null) {
			this.cuenta.add(cuenta);
		}else {
        	throw new NullPointerException();
		}
	}

	public Set<ICuenta> getCuentas() {
		return this.cuenta;
	}

	public String getNombre() {
		return this.nombre;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (cuenta == null) {
			if (other.cuenta != null)
				return false;
		} else if (!cuenta.equals(other.cuenta))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	/*public boolean equals(Cliente obj) {
		if ( this == obj && this.getNombre() == obj.getNombre() && this.getCuentas() == obj.getCuentas() ){
			return true;
		}else{
			return false;
		}
	}*/
	
}
