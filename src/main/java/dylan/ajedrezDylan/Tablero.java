package dylan.ajedrezDylan;

public class Tablero {
	private Casilla[][] tablero = new Casilla[8][8];

	public Tablero(Casilla[][] tablero) {
		super();
	    for (int i=0; i < this.tablero.length; i++) {
	        for (int j=0; j < this.tablero.length; j++) {
	        	this.tablero[i][j] = new Casilla(i, j);
	        }
	      }
		this.tablero = tablero;
	}
	
	
}