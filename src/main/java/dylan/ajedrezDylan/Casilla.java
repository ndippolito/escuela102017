package dylan.ajedrezDylan;

public class Casilla {
	private int x;
	private int y;
	private Pieza pieza;
	
	public Casilla(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	
	
	public Pieza getPieza() {
		return pieza;
	}



	public void setPieza(Pieza pieza) {
		this.pieza = pieza;
	}



	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}



	@Override
	public String toString() {
		return "Casilla [x=" + x + ", y=" + y + ", pieza=" + pieza + "]";
	}

	
	
}
