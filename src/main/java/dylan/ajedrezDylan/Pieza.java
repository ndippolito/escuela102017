package dylan.ajedrezDylan;

public abstract class Pieza {
	private String nombre;
	private Jugador jugador;
	protected Casilla casilla;
	
	
	
	public Pieza(Casilla casilla) {
		super();
		this.casilla = casilla;
	}

	public String getNombre() {
		return nombre;
	}
	
	public Jugador getJugador() {
		return jugador;
	}
	
	public Casilla getCasilla() {
		return casilla;
	}
	public void setCasilla(Casilla casilla) {
		this.casilla = casilla;
	}
	
	
	
}
