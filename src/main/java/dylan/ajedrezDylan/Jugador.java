package dylan.ajedrezDylan;

import java.util.List;

public class Jugador {
	private String nombre;
	private Jugador oponente;
	private boolean turnoActivo;
	private List<Pieza> piezas;
	
	
	public Jugador(String nombre, boolean turnoActivo, List<Pieza> piezas) {
		super();
		this.nombre = nombre;
		this.turnoActivo = turnoActivo = false;
		this.piezas = piezas;
	}

	public void moverPieza(Pieza pieza, Casilla casilla) {
		if (casilla.getPieza() == null) {
			pieza.setCasilla(casilla);
			casilla.setPieza(pieza);
			terminarTurno();
		} else {
			System.out.println("La casilla está ocupada");
		}
	}
	
	public void terminarTurno(){
		this.turnoActivo = false;
		this.oponente.turnoActivo = true;
	}
}
