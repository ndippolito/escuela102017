package dylan;

public class Persona {
	private long dni;
	private String nombre;
	private String domicilio;

	public Persona(long dni, String nombre, String domicilio) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.domicilio = domicilio;
	}

	public Persona(long dni) {
		super();
		this.dni = dni;
	}

	public long getDni() {
		return dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		if (!nombre.matches(".*\\d+.*")) {
			this.nombre = nombre;
		}
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	@Override
	public String toString() {
		return "Persona [dni=" + dni + ", nombre=" + nombre + ", domicilio=" + domicilio + "]";
	}

	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}

}
