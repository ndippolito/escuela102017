package dylan;

import dipi.ICuentaCorriente;
import dipi.MontoNegativoCuentaException;

public class CuentaCorriente extends Cuenta implements ICuentaCorriente {

	private long giroEnDescubierto;

	public CuentaCorriente(long saldo, Cliente cliente, long idCuenta, long giroEnDescubierto) {
		super(saldo, cliente, idCuenta);
		this.giroEnDescubierto = giroEnDescubierto;
	}
	public long giroEnDescubiertoHabilidado() {

		return this.giroEnDescubierto;
	}

	@Override
	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {
		if(montoAExtraer<= (this.getSaldo()+this.giroEnDescubierto)){
			long saldofinal=this.getSaldo()- montoAExtraer;
			CuentaCorriente cuenta = new CuentaCorriente(saldofinal, (Cliente)this.getCliente(), this.getId(), this.giroEnDescubierto);
			return true;
		} else{
			return false;
		}
	}
	public String toString() {
		return "CuentaCorriente [giroEnDescubierto=" + this.giroEnDescubierto + super.toString() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(CuentaCorriente.class)) {
			CuentaCorriente cuenta = (CuentaCorriente) obj;
			return (this.getId() == cuenta.getId()) &&
					(this.getCliente().equals(cuenta.getCliente())) &&
					(this.getSaldo() == cuenta.getSaldo()) &&
					(this.giroEnDescubierto == cuenta.giroEnDescubiertoHabilidado());
		} else {
			return false;
		}
	}

}
