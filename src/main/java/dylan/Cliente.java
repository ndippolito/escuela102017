package dylan;

import java.util.Set;

import dipi.ICliente;
import dipi.ICuenta;

public class Cliente implements ICliente {
	private String nombre;
	private Set<ICuenta> cuentas;
	
	
	public Cliente(String nombre, Set<ICuenta> cuentas) {
		super();
		this.nombre = nombre;
		this.cuentas = cuentas;
	}

	public void agregarCuenta(ICuenta cuenta) {
		cuentas.add(cuenta);
	}

	public Set<ICuenta> getCuentas() {
		return cuentas;
	}

	public String getNombre() {
		return this.nombre;
	}

	@Override
	public String toString() {
		return "Cliente [nombre=" + nombre + ", cuentas=" + cuentas + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(Cliente.class)) {
			Cliente cliente = (Cliente) obj;
			return (this.nombre == cliente.getNombre()) &&
					(this.cuentas.equals(cliente.getCuentas()));
		} else {
			return false;
		}
	}
	
	
	
}
