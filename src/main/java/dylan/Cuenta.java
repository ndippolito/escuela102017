package dylan;

import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public abstract class Cuenta implements ICuenta {

	private long saldo;
	private Cliente cliente;
	private long idCuenta;
	
	
	public Cuenta(long saldo, Cliente cliente, long idCuenta) {
		super();
		this.saldo = saldo;
		this.cliente = cliente;
		this.idCuenta = idCuenta;
	}
	
	public long getId() {
		return this.idCuenta;
	}
	
	public ICliente getCliente() {
		//preguntar
		return (ICliente)this.cliente;
	}
	
	public long getSaldo() {
		return this.saldo;
	}
	
	//preguntar
	public boolean depositar(long montoADepositar) throws MontoNegativoCuentaException {
		this.saldo+=montoADepositar;
		return true;
	}
	
	//preguntar
	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {
		if(montoAExtraer<=this.saldo) {
			this.saldo-=montoAExtraer;
			return true;
		}else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "[saldo=" + saldo + ", cliente=" + cliente.getNombre() + ", idCuenta=" + idCuenta + "]";
	}

	public boolean equals(Cuenta cuenta) {
		if((this.cliente.getNombre()==cuenta.getCliente().getNombre())&&(this.idCuenta==cuenta.getId())) {
			return true;
		} else {
			return false;
		}
	}

	
	
}
