package dylan;

import dipi.IBanco;
import dipi.ICliente;
import dipi.ICuenta;

import java.util.Set;

public class Banco implements IBanco { 
	private Set<ICliente> clientes;
	private Set<ICuenta> cuentas;
	private String nombre;
	public static int cuentasEnPanama2;
	
	public Banco(String nombre) {
		super();
		this.nombre = "";
		this.nombre = nombre;
		//deberia haber conflicto aca. 
		this.nombre = "" + this.nombre;
	}

	public void agregarCliente(ICliente cliente) {
		this.clientes.add(cliente);	
	}

	public Set<ICuenta> getCuentas() {
		return this.cuentas;
	}

	public Set<ICliente> getClientes() {
		return this.clientes;
	}

	public String getNombre() {
		return this.nombre;
	}

	@Override
	public String toString() {
		return "Banco [clientes=" + clientes + ", cuentas=" + cuentas + ", nombre=" + nombre + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(Banco.class)) {
			Banco banco = (Banco) obj;
			return (this.clientes == banco.getClientes()) &&
					(this.cuentas.equals(banco.getCuentas())) &&
					(this.nombre == banco.getNombre());
		} else {
			return false;
		}
	}
	
	
}
