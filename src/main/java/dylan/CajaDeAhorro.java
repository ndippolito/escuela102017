package dylan;

public class CajaDeAhorro extends Cuenta  {

	public CajaDeAhorro(long saldo, Cliente cliente, long idCuenta) {
		super(saldo, cliente, idCuenta);
	}

	@Override
	public String toString() {
		return "CajaDeAhorro " + super.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj==null) {
			return false;
		}
		if (this==obj) {
			return true;
		}
		if (obj.getClass().equals(CajaDeAhorro.class)) {
			CajaDeAhorro caja = (CajaDeAhorro) obj;
			return (this.getId() == caja.getId()) &&
					(this.getCliente().equals(caja.getCliente())) &&
					(this.getSaldo() == caja.getSaldo());
		} else {
			return false;
		}
	}
	
}
