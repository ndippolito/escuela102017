package jonathan.ajedrez;

public class Casillero {
	
	private Pieza piezaOcupante=null;
	
	public boolean estaOcupado() {
		return piezaOcupante!= null;
	}

	public boolean piezaDeIgualColor(Pieza otraPieza) {
		return piezaOcupante.getColor()==otraPieza.getColor();
	}
	
	public void setPieza(Pieza pieza) {
		this.piezaOcupante=pieza;
	}


	public Pieza getPieza() {

		return piezaOcupante;
	}
}
