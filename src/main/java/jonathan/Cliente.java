package jonathan;

import java.util.HashSet;
import java.util.Set;

import dipi.ICliente;
import dipi.ICuenta;

public class Cliente implements ICliente{

	
	private String nombre;	
	private Set<ICuenta> cuentas=new HashSet<ICuenta>();
	
	public void agregarCuenta(ICuenta cuenta) {
		this.cuentas.add(cuenta);
		
	}

	public Set<ICuenta> getCuentas() {
		return cuentas;
	}

	public String getNombre() {
		return nombre;
	}

}
