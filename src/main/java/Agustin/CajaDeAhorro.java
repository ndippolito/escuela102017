package Agustin;

import dipi.MontoNegativoCuentaException;
import Agustin.Cuenta;
import Agustin.Cliente;

public class CajaDeAhorro extends Cuenta {

	public CajaDeAhorro(long id, Cliente cliente, long saldo) throws IdNegativoCuentaException {
		super(id, cliente, saldo);
	}

	long saldo = getSaldo();

	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {

		if (montoAExtraer < 0)
			throw new MontoNegativoCuentaException();
		if (saldo - montoAExtraer < 0)
			return false;
		super.descontar(montoAExtraer);
		return true;

	}

	@Override
	public String toString() {
		return "CajaDeAhorro [saldo=" + saldo + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CajaDeAhorro other = (CajaDeAhorro) obj;
		if (saldo != other.saldo)
			return false;
		return true;
	}

}
