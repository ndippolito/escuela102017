package Agustin;

public class Persona {
	private int dni; 
	private String domicilio;
	private String nombre;
	
	public Persona(int dni, String domicilio, String nombre) {
		this.dni = dni;
		this.domicilio = domicilio;
		this.nombre = nombre;
	}
	
	public Persona(int dni) {
		this.dni = dni;
	}
	
	public int getDni() {
		return dni;
	}

	// public void setDni(int dni) {
	// this.dni = dni;
	// }

	public String getDomicilio() {
		return domicilio;
	}
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
	public boolean equals(Persona persona) {
		
		if (this == persona)
			return true;
		
		if (this.dni != persona.dni) {
			return false;
		}else if (this.domicilio != persona.domicilio) {
			return false;
		} else if(this.nombre != persona.nombre) {
			return false;
		} else {
			return true;
		}
			
	}

	@Override
	public String toString() {
		return "Persona [dni=" + dni + ", domicilio=" + domicilio + ", nombre=" + nombre + "]";
	}
	
	
}
