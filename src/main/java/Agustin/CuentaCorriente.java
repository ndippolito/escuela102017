package Agustin;

import dipi.ICuentaCorriente;
import dipi.MontoNegativoCuentaException;

import Agustin.Cuenta;
import Agustin.Cliente;

public class CuentaCorriente extends Cuenta implements ICuentaCorriente {

	private long giroEnDescubiertoHabilidado;

	public CuentaCorriente(long id, Cliente cliente, long saldo, long giro) throws IdNegativoCuentaException {
		super(id, cliente, saldo);
		this.giroEnDescubiertoHabilidado = giro;
	}

	public long giroEnDescubiertoHabilidado() {
		return giroEnDescubiertoHabilidado;
	}

	public boolean extraer(long montoAExtraer) throws MontoNegativoCuentaException {

		long saldo = this.getSaldo();

		if (montoAExtraer < 0)
			throw new MontoNegativoCuentaException();

		if ((saldo - montoAExtraer) < (giroEnDescubiertoHabilidado * (-1))) {
			return false;
		} else {
			super.descontar(montoAExtraer);
			return true;

		}
	}

	@Override
	public String toString() {
		return "CuentaCorriente [giroEnDescubiertoHabilidado=" + giroEnDescubiertoHabilidado + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CuentaCorriente other = (CuentaCorriente) obj;
		if (giroEnDescubiertoHabilidado != other.giroEnDescubiertoHabilidado)
			return false;
		return true;
	}

}
