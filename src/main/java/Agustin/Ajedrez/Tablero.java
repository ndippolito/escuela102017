package Agustin.Ajedrez;

import java.util.ArrayList;


public class Tablero {
	
	Pieza[][] tablero = new Pieza[8][8];

	

	public void colocarPiezas(ArrayList<Pieza> piezasBlancas, ArrayList<Pieza> piezasNegras) {
		for (Pieza pieza : piezasBlancas) {
			if(pieza instanceof Peon) {
				int fila = 1;
				for (int col = 0; col < 7 ; col++) {
					if(tablero[fila][col] == null) 
						tablero[fila][col] = pieza;
						pieza.setPosicionInicial(fila, col);
				}
			} else if(pieza instanceof Torre) {
				if(tablero[0][0] == null) {
					tablero[0][0] = pieza;
					pieza.setPosicionInicial(0, 0);
				} else {
					tablero[0][7] = pieza;
					pieza.setPosicionInicial(0, 7);
				}
			} else if(pieza instanceof Caballo) {
				if(tablero[0][1] == null) {
					tablero[0][1] = pieza;
					pieza.setPosicionInicial(0, 1);
				} else {
					tablero[0][6] = pieza;
					pieza.setPosicionInicial(0, 6);
				}
			} else if(pieza instanceof Alfil) {
				if(tablero[0][2] == null) {
					tablero[0][2] = pieza;
					pieza.setPosicionInicial(0, 2);
				} else {
					tablero[0][5] = pieza;
					pieza.setPosicionInicial(0, 5);
				}
			} else if(pieza instanceof Reina) {
				tablero[0][3] = pieza;
				pieza.setPosicionInicial(0, 3);
			} else {
				tablero[0][4] = pieza;
				pieza.setPosicionInicial(0, 4);
			}
			
			
		}
	}
	
	public void mostrarTablero() {
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < j; j++) {
				System.out.print(tablero[i][j].idPieza);
			}
			System.out.println(" ");
		}
	}

}
