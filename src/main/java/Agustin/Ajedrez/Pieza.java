package Agustin.Ajedrez;


public abstract class Pieza {
	private int filaActual;
	private int columnaActual;
	boolean enJuego;
	int idPieza;
	
	public int getFilaActual() {
		return filaActual;
	}

	public void setFilaActual(int filaActual) {
		this.filaActual = filaActual;
	}

	public int getColumnaActual() {
		return columnaActual;
	}

	public void setColumnaActual(int columnaActual) {
		this.columnaActual = columnaActual;
	}

	public boolean isEnJuego() {
		return enJuego;
	}

	public void setEnJuego(boolean enJuego) {
		this.enJuego = enJuego;
	}

	public int getIdPieza() {
		return idPieza;
	}

	public void setIdPieza(int idPieza) {
		this.idPieza = idPieza;
	}

	protected abstract boolean esMovimientoValido(Tablero tablero, int fila, int columna);

	public boolean mover(Tablero tablero,int id, int fila, int columna) {
		if (this.esMovimientoValido(tablero,fila, columna)) {
			this.filaActual = fila;
			this.columnaActual = columna;
			return true;
		} else {
			//HACE ALGO!!! 
			throw new RuntimeException("Implementa esto!");
		}
	}
	
	public void setPosicionInicial(int fila, int columna) {
		this.filaActual = fila;
		this.columnaActual = columna;
	}

	
	public void getPosicion() {
		System.out.println("Fila: "+filaActual+" Columna: " +columnaActual);
	}
	
	
}
