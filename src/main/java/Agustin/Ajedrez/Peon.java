package Agustin.Ajedrez;

public class Peon extends Pieza {
	
	public Peon(int i) {
	}

	@Override
	protected boolean esMovimientoValido(Tablero tablero,int fila, int columna) {
		if(fila < 0 || fila > 7 || columna < 0 || columna > 7) return false;
		if(!enJuego) return false;
		if(tablero.tablero[fila][columna] != null) {
			System.out.println("No esta vacio");
			return false;
		}
		if(fila - getFilaActual() != 1 || columna != getColumnaActual()) {
			return false;
		}
		return true;
	}



}
