package Agustin.Ajedrez;


public class Caballo extends Pieza {
	public Caballo(int i) {
		idPieza = i;
		}

	@Override
	protected boolean esMovimientoValido(Tablero tablero, int fila, int columna) {
		if(fila < 0 || fila > 7 || columna < 0 || columna > 7) return false;
		if(!enJuego) return false;
		if(tablero.tablero[fila][columna] != null) {
			System.out.println("No esta vacio");
			return false;
		}
		int filaActual = getFilaActual();
		int columnaActual = getColumnaActual();
		
		if((Math.abs(filaActual - fila) == 2 && Math.abs(columnaActual - columna) == 1) || 
				(Math.abs(filaActual - fila) == 1 && Math.abs(columnaActual - columna) == 2)) {
			return true;
		}
		
		
		return false;
	}


}
