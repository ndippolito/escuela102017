package Agustin;

import java.util.HashSet;
import java.util.Set;
import Agustin.Cliente;

import dipi.IBanco;
import dipi.ICliente;
import dipi.ICuenta;

public class Banco implements IBanco {

	private Set<ICliente> clientes = new HashSet<ICliente>();
	private String nombre;

	public Banco(String nombre) {
		if (Cliente.esNombreValido(nombre)) {
			this.nombre = nombre;
		} else {
			clientes = null;
		}
	}

	public void agregarCliente(ICliente cliente) {
		clientes.add(cliente);
	}

	public Set<ICuenta> getCuentas() {
		Set<ICuenta> cuentas = new HashSet<ICuenta>();
		for (ICliente cliente : clientes) {
			cuentas.addAll(cliente.getCuentas());
		}
		return cuentas;
	}

	public Set<ICliente> getClientes() {
		return this.clientes;
	}

	public String getNombre() {
		return this.nombre;
	}

	@Override
	public String toString() {
		return "Banco [clientes=" + clientes + ", nombre=" + nombre + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Banco other = (Banco) obj;
		if (clientes == null) {
			if (other.clientes != null)
				return false;
		} else if (!clientes.equals(other.clientes))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}
