package Agustin;

import Agustin.Cliente;
import dipi.ICliente;
import dipi.ICuenta;
import dipi.MontoNegativoCuentaException;

public abstract class Cuenta implements ICuenta {

	private long id;
	private Cliente cliente;
	private long saldo;

	// Un cliente no puede tener 2 cuentas con el mismo id
	public Cuenta(long id, Cliente cliente, long saldo) throws IdNegativoCuentaException {
		if (id <= 0)
			throw new IdNegativoCuentaException();
		this.id = id;
		this.cliente = cliente;
		this.saldo = saldo;
		cliente.agregarCuenta(this);

		// if (cliente.esIdValido(id)) {
		// this.id = id;
		// this.cliente = cliente;
		// this.saldo = saldo;
		// cliente.agregarCuenta(this);
		// }
	}

	public long getId() {
		return this.id;
	}

	public ICliente getCliente() {
		return this.cliente;
	}

	public long getSaldo() {
		return this.saldo;
	}

	public boolean depositar(long montoADepositar) throws MontoNegativoCuentaException {
		if (montoADepositar < 0)
			throw new MontoNegativoCuentaException();
		this.saldo += montoADepositar;
		return true;
	}

	public void descontar(long monto) {
		this.saldo -= monto;
	}

	@Override
	public String toString() {
		return "Cuenta [id=" + id + ", cliente=" + cliente + ", saldo=" + saldo + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuenta other = (Cuenta) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (id != other.id)
			return false;
		if (saldo != other.saldo)
			return false;
		return true;
	}

}
