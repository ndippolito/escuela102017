package Agustin;

import java.util.HashSet;
import java.util.Set;

import dipi.ICliente;
import dipi.ICuenta;

public class Cliente implements ICliente {

	private Set<ICuenta> cuentas = new HashSet<ICuenta>();
	private String nombre;

	// Se puede crear un cliente sin cuenta, pero no una cuenta sin cliente
	// No puede contener numero el nombre

	public Cliente(String nombre) {
		if (esNombreValido(nombre)) {
			this.nombre = nombre;
		} else {
			cuentas = null;
		}
	}

	// Preguntar por este metodo
	public void agregarCuenta(ICuenta cuenta) {
		if (!cuentas.contains(cuenta)) {
			this.cuentas.add(cuenta);
		}
	}

	public Set<ICuenta> getCuentas() {
		return cuentas;

	}

	public String getNombre() {
		return this.nombre;
	}

	public static boolean esNombreValido(String nombre) {
		if (nombre == null)
			return false;
		if (nombre.isEmpty())
			return false;
		char[] nombreArray = nombre.toCharArray();
		for (char c : nombreArray) {
			if (Character.isDigit(c))
				return false;
		}
		return true;
	}

	public boolean esIdValido(long id) {
		for (ICuenta cuenta : this.cuentas) {
			if (cuenta.getId() == id)
				return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Cliente [cuentas=" + cuentas + ", nombre=" + nombre + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (cuentas == null) {
			if (other.cuentas != null)
				return false;
		} else if (!cuentas.equals(other.cuentas))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}
