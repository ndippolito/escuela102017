package dipi;

import java.util.Set;

import sam.Cuenta;

public interface ICliente {
	public void agregarCuenta(ICuenta cuenta);
	public Set<Cuenta> getCuentas();
	public String getNombre();
}
