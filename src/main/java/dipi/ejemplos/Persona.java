package dipi.ejemplos;

public class Persona implements ISuperHeroe {
	private String nombre;
	private Long dni;
	private String poderes;

	public Persona(String nombre) {
		this.nombre = nombre;
	}
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Este metodo cambia el nombre de la persona. 
	 * Si el parametro <code>nombre<code> es invalido, 
	 * el metodo no cambia el nombre de la persona. 
	 *  
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getDni() {
		return dni;
	}
	public String getPoderes() {
		return this.poderes;
	}
	public void setPoderes(String poderes) {
		this.poderes = poderes;
	}
}
