package dipi.ejemplos;

import java.io.IOException;

public class HinchaDeRiver extends Persona implements ISuperHeroeVolador {

	private Double cantidadDePlumas;

	public HinchaDeRiver(String nombre) {
		super(nombre);
	}
	
	public Double getCantidadDePlumas() {
		return cantidadDePlumas;
	}

	public void setCantidadDePlumas(Double cantidadDePlumas) 
					throws IOException {
		this.cantidadDePlumas = cantidadDePlumas;
	}

	@Override
	public String getPoderes() {
		return super.getPoderes() + "HinchaDeRiver";
	}

	public void getColorDeLaCapa() {
		// TODO Auto-generated method stub
		
	}

}
