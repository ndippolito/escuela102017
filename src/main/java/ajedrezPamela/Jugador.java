package ajedrezPamela;

import java.util.HashSet;
import java.util.Set;

public class Jugador {
	
	private String nombre;
	private String colorFiguras;	
	
	public Jugador(String colorFiguras) {
		this.colorFiguras=colorFiguras;
	}
	
	public boolean MoverFigura(Tablero tablero,int filaOrigen, int columnaOrigen,int filaDestino, int columnaDestino) {
		
		if(tablero.getTablero()[filaOrigen][columnaDestino] instanceof Figura)
		{
			Figura figura=tablero.getTablero()[filaOrigen][columnaDestino];
			
			if(figura.getColor()==this.colorFiguras) {				
				if (figura.verificarDisponibilidadCasilla(tablero,filaDestino, columnaDestino) && figura.VerificarLimiteMovimiento(filaDestino, columnaDestino)) {
					tablero.getTablero()[filaDestino][columnaDestino]=figura;
					tablero.getTablero()[filaOrigen][columnaOrigen]=null;
					return true;
				}	
			}
		}
			
		return false;
	}
	
	public void CAmbiarDisponibilidadFigura(Figura figuraEliminada) {
		figuraEliminada.cambiarDisponibilidad();
	}
}
