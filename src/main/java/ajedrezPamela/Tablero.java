package ajedrezPamela;

import java.util.HashSet;
import java.util.Set;

public class Tablero {
	
	private Figura tablero[][] = new Figura[8][8];
	
	public Tablero() {
		this.InicializarTablero();
	}
	
	public void InicializarTablero() {
		
		for (int j = 0; j < this.tablero.length; j++) {
			this.tablero[1][j]= new Alfil("Blanco");
			this.tablero[6][j]= new Alfil("Negro");
		}	
	}
	
	public Figura[][] getTablero() {
		return this.tablero;
	}
	
}
