package ajedrezPamela;

public class Figura {
	
	private String color;
	private boolean disponible;
	protected int ubicacion;
	
	public Figura(String color) {
		this.disponible=true;
		this.color=color;
	}
	
	public void cambiarDisponibilidad() {
		this.disponible = false;
	}
	
	public boolean VerificarLimiteMovimiento(int fila, int columna) {
		return ( ( 0 <= fila && fila < 8 ) && ( 0 <= columna && columna< 8) ) ? true : false;
	}
	
	
	public boolean verificarDisponibilidadCasilla(Tablero tablero,int fila, int columna) {
		
		boolean disponible= false;
		
		if((tablero.getTablero()[fila][columna] instanceof Figura && 
				(!tablero.getTablero()[fila][columna].color.equals(this.color)) ) || 
				tablero.getTablero()[fila][columna] == null ) 
		{
			disponible = true;
		}
		
		return disponible;
	}
	
	public String getColor() {
		return this.color;
	}
}
